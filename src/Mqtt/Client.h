/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <memory>

#include <QObject>
#include <QMqttClient>

namespace Zax {
namespace Mqtt {

class Client : public QMqttClient
{
    Q_OBJECT

public:
    Client();
    ~Client() override;

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(bool connected READ connected NOTIFY connectedChanged)
    bool connected() const;
    Q_SIGNAL void connectedChanged();

    static std::shared_ptr<Client> instance();

private:
    class Private;
    const std::unique_ptr<Private> d;
};

}
}
