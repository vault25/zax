/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "Subscription.h"

using namespace Zax::Mqtt;

Subscription::Subscription(QObject* parent)
    : QObject(parent)
{

}

QString Subscription::topic() const
{
    return m_topic;
}

void Subscription::setTopic(const QString & newTopic)
{
    if (newTopic == m_topic) {
        return;
    }

    m_topic = newTopic;

    Q_EMIT topicChanged();
}

QVariant Subscription::value() const
{
    return m_value;
}

bool Subscription::enabled() const
{
    return m_enabled;
}

void Subscription::setEnabled(bool newEnabled)
{
    if (newEnabled == m_enabled) {
        return;
    }

    m_enabled = newEnabled;
    Q_EMIT enabledChanged();
}
