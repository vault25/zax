/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <QObject>
#include <QVariant>

#include "zaxmqtt_export.h"

namespace Zax {
namespace Mqtt {

class ZAXMQTT_EXPORT Subscription : public QObject
{
    Q_OBJECT

public:
    Subscription(QObject* parent = nullptr);

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QString topic READ topic WRITE setTopic NOTIFY topicChanged)
    QString topic() const;
    void setTopic(const QString & newTopic);
    Q_SIGNAL void topicChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QVariant value READ value NOTIFY valueChanged)
    QVariant value() const;
    Q_SIGNAL void valueChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)
    bool enabled() const;
    void setEnabled(bool newEnabled);
    Q_SIGNAL void enabledChanged();

private:
    QString m_topic;
    QVariant m_value;
    bool m_enabled;
};

}
}
