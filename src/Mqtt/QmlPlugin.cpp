/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "QmlPlugin.h"

#include <QQmlEngine>

#include "Subscription.h"

using namespace Zax::Mqtt;

void QmlPlugin::registerTypes(const char* uri)
{
    Q_ASSERT(QLatin1String(uri) == QLatin1String("Zax.Mqtt"));

    qmlRegisterType<Subscription>(uri, 1, 0, "Subscription");
}

void QmlPlugin::initializeEngine(QQmlEngine* engine, const char* uri)
{
    Q_ASSERT(QLatin1String(uri) == QLatin1String("Zax.Mqtt"));

    Q_UNUSED(engine)
}
