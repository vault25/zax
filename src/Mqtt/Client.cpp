/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "Client.h"

using namespace Zax::Mqtt;

class Q_DECL_HIDDEN Client::Private
{
public:

};

Client::Client()
    : QMqttClient()
{
}

Client::~Client() = default;

bool Client::connected() const
{
    return false; // d->connected;
}

std::shared_ptr<Client> Zax::Mqtt::Client::instance()
{
    static auto instance = std::make_shared<Client>();
    return instance;
}
