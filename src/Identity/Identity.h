/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <memory>

#include <QObject>
#include <QVariantMap>

#include "IdentityProvider.h"

#include "identity_export.h"

namespace Zax {
namespace Identity {

class IDENTITY_EXPORT Identity : public QObject
{
    Q_OBJECT

public:
    Identity(QObject* parent = nullptr);
    ~Identity() override;

    Q_SIGNAL void changed();

    Q_PROPERTY(QString id READ id WRITE setId NOTIFY changed)
    QString id() const;
    void setId(const QString& id);

    Q_PROPERTY(QString name READ name NOTIFY changed)
    QString name() const;

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QVariantMap properties READ properties NOTIFY changed)
    QVariantMap properties() const;

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(bool valid READ valid NOTIFY changed)
    bool valid() const;

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(IdentityProvider* provider READ provider CONSTANT)
    IdentityProvider* provider() const;
    void setProvider(std::unique_ptr<IdentityProvider>&& newProvider);

    Q_SIGNAL void error(int errorCode, const QString& errorString);

    static std::shared_ptr<Identity> self();

private:
    void onSelected(const QVariantMap& properties);

    class Private;
    const std::unique_ptr<Private> d;
};

} //namespace Identity
} //namespace Zax
