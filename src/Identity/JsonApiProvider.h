/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <memory>

#include "IdentityProvider.h"

#include "identity_export.h"

namespace Zax {
namespace Identity {

class IDENTITY_EXPORT JsonApiProvider : public IdentityProvider
{
    Q_OBJECT
public:
    JsonApiProvider(const QString& path, const QString& type, QObject* parent = nullptr);
    ~JsonApiProvider() override;

    void create(const QVariantMap& properties) override;
    void select(const QString& id) override;
    void checkAvailable(const QVariantMap& properties) override;

private:
    void onQuerySuccess();
    void onQueryError();

    void onPostSuccess();
    void onPostError();

    class Private;
    const std::unique_ptr<Private> d;
};

} // namespace Identity
} // namespace Zax
