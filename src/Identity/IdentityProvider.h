/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <QObject>

#include "identity_export.h"

namespace Zax {
namespace Identity {

class IDENTITY_EXPORT IdentityProvider : public QObject
{
    Q_OBJECT

public:
    IdentityProvider(QObject* parent = nullptr);

    QString idProperty() const;
    void setIdProperty(const QString& newIdProperty);

    QString nameProperty() const;
    void setNameProperty(const QString& newNameProperty);

    virtual void create(const QVariantMap& properties) = 0;
    virtual void select(const QString& id) = 0;
    virtual void checkAvailable(const QVariantMap& properties) = 0;

    Q_SIGNAL void available(const QVariantMap& properties);
    Q_SIGNAL void notAvailable(const QVariantMap& properties);

    Q_SIGNAL void selected(const QVariantMap& properties);
    Q_SIGNAL void error(int errorCode, const QString& message);

private:
    QString m_idProperty = QStringLiteral("id");
    QString m_nameProperty = QStringLiteral("name");
};

} // namespace Identity
} // namespace Zax
