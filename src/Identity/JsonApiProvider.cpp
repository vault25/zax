/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "JsonApiProvider.h"

#include <QJsonObject>
#include <QJsonArray>

#include "JsonApi/Api.h"
#include "JsonApi/Document.h"
#include "JsonApi/Query.h"

#include "identity_logging.h"

using namespace Zax;
using namespace Zax::Identity;
using namespace Qt::StringLiterals;

class JsonApiProvider::Private
{
public:
    QString path;
    QString type;

    std::unique_ptr<JsonApi::Query> query;
    std::unique_ptr<JsonApi::ApiResult> postResult;
    std::unique_ptr<JsonApi::ApiResult> checkResult;
};

JsonApiProvider::JsonApiProvider(const QString& path, const QString& type, QObject* parent)
    : IdentityProvider(parent)
    , d(std::make_unique<Private>())
{
    d->path = path;
    d->type = type;

    d->query = std::make_unique<JsonApi::Query>();
    connect(d->query.get(), &JsonApi::Query::success, this, &JsonApiProvider::onQuerySuccess);
    connect(d->query.get(), &JsonApi::Query::error, this, &JsonApiProvider::onQueryError);
}

JsonApiProvider::~JsonApiProvider() = default;

void JsonApiProvider::create(const QVariantMap& properties)
{
    auto data = QJsonObject{
        {u"data"_s, QJsonObject{
            {u"type"_s, d->type},
            {u"attributes"_s, QJsonObject::fromVariantMap(properties)}
        }}
    };

    d->postResult = JsonApi::Api::instance()->post(d->path, data);
    d->postResult->setValidationCallback(JsonApi::Api::validateJsonApi);
    connect(d->postResult.get(), &JsonApi::ApiResult::success, this, &JsonApiProvider::onPostSuccess);
    connect(d->postResult.get(), &JsonApi::ApiResult::error, this, &JsonApiProvider::onPostError);
}

void JsonApiProvider::select(const QString& id)
{
    if (!d->query) {
        d->query = std::make_unique<JsonApi::Query>();
    }

    d->query->setPath(d->path + QLatin1Char('/') + id);
    d->query->run();
}

void JsonApiProvider::checkAvailable(const QVariantMap& properties)
{
    QString filter;
    if (properties.contains(nameProperty())) {
        auto nameValue = properties.value(nameProperty()).toString();
        if (nameValue.isEmpty()) {
            Q_EMIT notAvailable(properties);
            return;
        }

        filter = QStringLiteral("filter[%1][value]=%2").arg(nameProperty(), properties.value(nameProperty()).toString());
    }

    d->checkResult = JsonApi::Api::instance()->get(d->path, filter, JsonApi::Cache::CacheMode::DoNotCache);
    d->checkResult->setValidationCallback([](JsonApi::ApiResult* result) -> bool {
        auto document = result->asJson().value_or(QJsonDocument{});
        if (document.isEmpty() || !document.isObject()) {
            result->setErrorString(QStringLiteral("Empty JSON document received"));
            return false;
        }

        return true;
    });
    connect(d->checkResult.get(), &JsonApi::ApiResult::success, [this, properties]() {
        auto data = d->checkResult->asJson().value_or(QJsonDocument{})[u"data"];
        if (data.isObject() || (data.isArray() && !data.toArray().isEmpty())) {
            Q_EMIT notAvailable(properties);
        } else {
            Q_EMIT available(properties);
        }
    });
    connect(d->checkResult.get(), &JsonApi::ApiResult::error, this, [this, properties]() {
        Q_EMIT error(d->checkResult->errorCode(), d->checkResult->errorString());
        Q_EMIT notAvailable(properties);
    });
}

void JsonApiProvider::onQuerySuccess()
{
    Q_EMIT selected(d->query->entries().first().attributeValues());
}

void JsonApiProvider::onQueryError()
{
    qCDebug(IDENTITY) << "Error selecting identity" << d->query->result()->errorString();
    Q_EMIT error(d->query->result()->errorCode(), d->query->result()->errorString());
}

void JsonApiProvider::onPostSuccess()
{
    auto entry = d->postResult->asJson().value()[u"data"].toObject();

    Q_EMIT selected(JsonApi::Api::jsonApiToVariantMap(entry));
}

void JsonApiProvider::onPostError()
{
    qCDebug(IDENTITY) << "Error when creating new identity" << d->postResult->errorString();
    Q_EMIT error(d->postResult->errorCode(), d->postResult->errorString());
}
