/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <memory>

#include <QObject>
#include <QVariantMap>

#include "identity_export.h"

namespace Zax {
namespace Identity {

class IDENTITY_EXPORT Builder : public QObject
{
    Q_OBJECT

public:
    Builder(QObject* parent = nullptr);
    ~Builder() override;

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QVariantMap properties READ properties WRITE setProperties NOTIFY propertiesChanged)
    QVariantMap properties() const;
    void setProperties(const QVariantMap& newProperties);
    Q_SIGNAL void propertiesChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(bool available READ available NOTIFY availableChanged)
    bool available() const;
    Q_SIGNAL void availableChanged();

    Q_INVOKABLE void create();

    Q_SIGNAL void created();
    Q_SIGNAL void error();

private:
    void checkAvailable();

    class Private;
    const std::unique_ptr<Private> d;
};

} // namespace Identity
} // namespace Zax
