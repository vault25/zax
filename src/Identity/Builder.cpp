/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "Builder.h"

#include "Identity.h"
#include "IdentityProvider.h"

using namespace Zax;
using namespace Zax::Identity;

class IDENTITY_NO_EXPORT Builder::Private
{
public:
    void checkAvailable();

    QVariantMap properties;
    bool available;
};

Builder::Builder(QObject* parent)
    : QObject(parent)
    , d(std::make_unique<Private>())
{
}

Builder::~Builder() = default;

QVariantMap Builder::properties() const
{
    return d->properties;
}

void Builder::setProperties(const QVariantMap & newProperties)
{
    if (newProperties == d->properties) {
        return;
    }

    d->properties = newProperties;
    checkAvailable();
    Q_EMIT propertiesChanged();
}

bool Builder::available() const
{
    return d->available;
}

void Builder::create()
{
    auto provider = Identity::self()->provider();

    connect(provider, &IdentityProvider::selected, this, [this]() {
        Identity::self()->provider()->disconnect(this);
        Q_EMIT created();
    });
    connect(provider, &IdentityProvider::error, this, [this]() {
        Identity::self()->provider()->disconnect(this);
        Q_EMIT error();
    });

    Identity::self()->provider()->create(d->properties);
}

void Builder::checkAvailable()
{
    if (d->properties.isEmpty()) {
        if (d->available) {
            d->available = false;
            Q_EMIT availableChanged();
        }
        return;
    }

    auto provider = Identity::self()->provider();

    connect(provider, &IdentityProvider::available, this, [this](const QVariantMap& properties) {
        if (properties != d->properties) {
            return;
        }

        Identity::self()->provider()->disconnect(this);
        d->available = true;
        Q_EMIT availableChanged();
    });

    connect(provider, &IdentityProvider::notAvailable, this, [this](const QVariantMap& properties) {
        if (properties != d->properties) {
            return;
        }

        Identity::self()->provider()->disconnect(this);
        d->available = false;
        Q_EMIT availableChanged();
    });

    provider->checkAvailable(d->properties);
}
