/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "Identity.h"

#include "IdentityProvider.h"
#include "identity_logging.h"

using namespace Zax::Identity;

class IDENTITY_NO_EXPORT Identity::Private
{
public:
    QString id;
    QString name;
    QVariantMap properties;

    std::unique_ptr<IdentityProvider> provider = nullptr;
};

Identity::Identity(QObject* parent)
    : QObject(parent)
    , d(new Private)
{
}

Identity::~Identity()
{
}

QString Identity::id() const
{
    return d->id;
}

void Identity::setId(const QString & newId)
{
    if (newId == d->id) {
        return;
    }

    d->provider->select(newId);
}

QString Identity::name() const
{
    return d->name;
}

QVariantMap Identity::properties() const
{
    return d->properties;
}

bool Identity::valid() const
{
    return !d->id.isEmpty();
}

IdentityProvider* Identity::provider() const
{
    return d->provider.get();
}

void Identity::setProvider(std::unique_ptr<IdentityProvider>&& newProvider)
{
    if (newProvider == d->provider) {
        return;
    }

    if (d->provider) {
        d->provider->disconnect(this);
    }

    d->provider = std::move(newProvider);
    if (d->provider) {
        connect(d->provider.get(), &IdentityProvider::selected, this, &Identity::onSelected);
        connect(d->provider.get(), &IdentityProvider::error, this, &Identity::error);
    }
}

std::shared_ptr<Identity> Identity::self()
{
    static auto self = std::make_shared<Identity>();
    return self;
}

void Identity::onSelected(const QVariantMap& properties)
{
    d->properties = properties;
    d->id = d->properties.value(d->provider->idProperty()).toString();
    d->name = d->properties.value(d->provider->nameProperty()).toString();

    qCDebug(IDENTITY) << "Selected identity" << d->id << d->name;

    Q_EMIT changed();
}
