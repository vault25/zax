/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "QmlPlugin.h"

#include <QQmlEngine>

#include "Identity.h"
#include "Builder.h"

using namespace Zax::Identity;

void QmlPlugin::registerTypes(const char* uri)
{
    Q_ASSERT(QLatin1String(uri) == QLatin1String("Zax.Identity"));

    qmlRegisterSingletonInstance(uri, 1, 0, "Self", Identity::self().get());
    qmlRegisterType<Builder>(uri, 1, 0, "Builder");
}
