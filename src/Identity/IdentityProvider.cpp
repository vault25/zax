/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "IdentityProvider.h"

using namespace Zax::Identity;

IdentityProvider::IdentityProvider(QObject* parent)
    : QObject(parent)
{
}

QString IdentityProvider::idProperty() const
{
    return m_idProperty;
}

void IdentityProvider::setIdProperty(const QString& newIdProperty)
{
    m_idProperty = newIdProperty;
}

QString IdentityProvider::nameProperty() const
{
    return m_nameProperty;
}

void IdentityProvider::setNameProperty(const QString& newNameProperty)
{
    m_nameProperty = newNameProperty;
}
