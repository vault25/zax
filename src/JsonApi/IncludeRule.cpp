/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "IncludeRule.h"

#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include "Api.h"
#include "Document.h"

using namespace Zax::JsonApi;

std::optional<QJsonValue> findInclude(const QJsonArray& includes, const QString& type, const QString& id)
{
    auto itr = std::find_if(includes.begin(), includes.end(), [id, type](auto value) {
        auto object = value.toObject();
        return object[u"id"].toString() == id && object[u"type"] == type;
    });

    if (itr != includes.end()) {
        return *itr;
    }

    return std::nullopt;
}

IncludeRule::IncludeRule(QObject* parent)
    : QueryRule(parent)
{
}

void IncludeRule::modifyQuery(QUrlQuery& query)
{
    query.addQueryItem(QStringLiteral("include"), m_field);
}

void IncludeRule::modifyResult(const QJsonDocument& document, QList<Document>& output)
{
    auto includes = document.object().value(u"included").toArray();

    for (auto& item : output) {
        const auto relationships = item.relationships();
        const auto relationshipValues = item.relationshipValues();
        for (auto [name, type] : relationships.asKeyValueRange()) {
            auto value = relationshipValues.value(name);
            if (value.typeId() == QMetaType::QVariantList) {
                auto list = value.toList();
                auto entries = QJsonArray{};
                for (auto id : list) {
                    auto include = findInclude(includes, type.toString(), id.toString());
                    if (include.has_value()) {
                        entries.append(include.value());
                    }
                }
                item.setAttributeValue(name, QJsonValue{entries});
            } else {
                auto include = findInclude(includes, type.toString(), value.toString());
                if (include.has_value()) {
                    item.setAttributeValue(name, include.value());
                }
            }
        }
    }
}

QString IncludeRule::field() const
{
    return m_field;
}

void IncludeRule::setField(const QString& newField)
{
    if (newField == m_field) {
        return;
    }

    m_field = newField;
    Q_EMIT fieldChanged();
    Q_EMIT changed();
}
