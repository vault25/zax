/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <chrono>

#include <QObject>
#include <QQmlListProperty>
#include <QQmlParserStatus>
#include <qqmlregistration.h>

#include "jsonapi_export.h"

namespace Zax {
namespace JsonApi {

class ApiResult;
class Document;
class QueryRule;

/**
 * @todo write docs
 */
class JSONAPI_EXPORT Query : public QObject, public QQmlParserStatus
{
    Q_OBJECT
    QML_ELEMENT
    Q_CLASSINFO("DefaultProperty", "rules")
    Q_INTERFACES(QQmlParserStatus)

public:
    Query(QObject* parent = nullptr);
    ~Query() override;

    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)
    bool enabled() const;
    void setEnabled(bool newEnabled);
    Q_SIGNAL void enabledChanged();

    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    QString path() const;
    void setPath(const QString& newPath);
    Q_SIGNAL void pathChanged();

    Q_PROPERTY(QQmlListProperty<Zax::JsonApi::QueryRule> rules READ rulesProperty NOTIFY rulesChanged)
    QQmlListProperty<QueryRule> rulesProperty();
    QVector<QueryRule*> rules() const;
    void setRules(const QVector<QueryRule*>& newRules);
    Q_SIGNAL void rulesChanged();

    Q_PROPERTY(bool cacheResults READ cacheResults WRITE setCacheResults NOTIFY cacheResultsChanged)
    bool cacheResults() const;
    void setCacheResults(bool cache);
    Q_SIGNAL void cacheResultsChanged();

    Q_PROPERTY(int retryCount READ retryCount WRITE setRetryCount NOTIFY retryCountChanged)
    int retryCount() const;
    void setRetryCount(int newRetryCount);
    Q_SIGNAL void retryCountChanged();

    Q_PROPERTY(int retryInterval READ retryInterval WRITE setRetryInterval NOTIFY retryIntervalChanged)
    int retryInterval() const;
    void setRetryInterval(int newRetryInterval);
    void setRetryInterval(std::chrono::milliseconds newRetryInterval);
    Q_SIGNAL void retryIntervalChanged();

    Q_PROPERTY(int refreshInterval READ refreshInterval WRITE setRefreshInterval NOTIFY refreshIntervalChanged)
    int refreshInterval() const;
    void setRefreshInterval(int newRefreshInterval);
    void setRefreshInterval(std::chrono::milliseconds newRefreshInterval);
    Q_SIGNAL void refreshIntervalChanged();

    Q_PROPERTY(bool refreshOnChange READ refreshOnChange WRITE setRefreshOnChange NOTIFY refreshOnChangeChanged)
    bool refreshOnChange() const;
    void setRefreshOnChange(bool newRefreshOnChange);
    Q_SIGNAL void refreshOnChangeChanged();

    Q_PROPERTY(QVariantMap attributes READ attributes WRITE setAttributes NOTIFY attributesChanged)
    QVariantMap attributes() const;
    void setAttributes(const QVariantMap& newAttributes);
    Q_SIGNAL void attributesChanged();

    Q_PROPERTY(QVariantMap relationships READ relationships WRITE setRelationships NOTIFY relationshipsChanged)
    QVariantMap relationships() const;
    void setRelationships(const QVariantMap& newRelationships);
    Q_SIGNAL void relationshipsChanged();

    Q_PROPERTY(bool running READ running WRITE setRunning NOTIFY runningChanged)
    bool running() const;
    void setRunning(bool newRunning);
    Q_SIGNAL void runningChanged();

    Q_PROPERTY(QList<Document> entries READ entries NOTIFY entriesChanged)
    QList<Document> entries() const;
    Q_SIGNAL void entriesChanged();

    Q_INVOKABLE void run();
    Q_INVOKABLE void refresh();

    Q_INVOKABLE void removeFromCache();

    ApiResult* result() const;

    Q_SIGNAL void changed();
    Q_SIGNAL void success();
    Q_SIGNAL void error();

    void classBegin() override;
    void componentComplete() override;

private:
    void onSuccess();
    void onError();
    void onChanged();

    static void appendRule(QQmlListProperty<QueryRule>* property, QueryRule* rule);
    static qsizetype ruleCount(QQmlListProperty<QueryRule>* property);
    static QueryRule* ruleAt(QQmlListProperty<QueryRule>* property, qsizetype index);
    static void clearRules(QQmlListProperty<QueryRule>* property);
    static void replaceRule(QQmlListProperty<QueryRule>* property, qsizetype index, QueryRule* rule);
    static void removeLastRule(QQmlListProperty<QueryRule>* property);

    class Private;
    const std::unique_ptr<Private> d;
};

} // namespace JsonApi
} // namespace Zax

QDebug operator<<(QDebug debug, const Zax::JsonApi::Query* input);
QDebug operator<<(QDebug debug, const std::unique_ptr<Zax::JsonApi::Query>& input);
QDebug operator<<(QDebug debug, const std::shared_ptr<Zax::JsonApi::Query>& input);
