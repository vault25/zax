/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <optional>
#include <chrono>

#include <QObject>
#include <QLoggingCategory>
#include <QJsonDocument>
#include <QNetworkReply>
#include <qqmlregistration.h>

#include "ApiResult.h"
#include "Cache.h"

#include "jsonapi_export.h"

class QNetworkReply;

namespace Zax {
namespace JsonApi {

class JSONAPI_EXPORT Api : public QObject
{
    Q_OBJECT

public:
    Api(QObject* parent = nullptr);
    ~Api();

    bool isStarted() const;
    Q_SIGNAL void started();
    Q_SIGNAL void stopped();

    bool isConnected() const;
    Q_SIGNAL void connected();

    std::chrono::milliseconds timeout() const;
    void setTimeout(std::chrono::milliseconds newTimeout);

    Cache* cache() const;

    [[nodiscard]] ApiResultPtr get(QStringView path, QStringView filters, Cache::CacheMode cache = Cache::CacheMode::Cache);
    [[nodiscard]] ApiResultPtr get(const QUrl& url, Cache::CacheMode cache = Cache::CacheMode::Cache);

    [[nodiscard]] ApiResultPtr post(QStringView path, const QJsonObject& data, const QString &query = QString{});
    [[nodiscard]] ApiResultPtr patch(QStringView path, const QJsonObject& data, const QString &query = QString{});
    [[nodiscard]] ApiResultPtr del(QStringView path, const QJsonObject& data, const QString &query = QString{});

    QStringList servers() const;
    void setServers(const QStringList& servers);

    QString connectedServer() const;

    QString apiPath() const;
    void setApiPath(const QString& newApiPath);

    QString userName() const;
    void setUserName(const QString& userName);

    QString password() const;
    void setPassword(const QString& password);

    void start();
    void stop();

    static std::shared_ptr<Api> instance();

    static QVariantMap jsonApiToVariantMap(const QJsonObject& apiObject);

    static bool validateJsonApi(ApiResult* result);

private:
    friend ApiResult;
    friend class Query;

    QNetworkReply* getFromRemote(const QUrl& url);
    QUrl buildUrl(QStringView path, QStringView query);

    class Private;
    const std::unique_ptr<Private> d;
};

} // namespace JsonApi
} // namespace Zax
