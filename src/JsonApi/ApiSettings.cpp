// SPDX-FileCopyrightText: 2023 Arjen Hiemstra <ahiemstra@heimr.nl>
// 
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "ApiSettings.h"

#include "Api.h"

using namespace Zax::JsonApi;

ApiSettings::ApiSettings(QObject* parent)
    : QObject(parent)
{
    connect(Api::instance().get(), &Api::started, this, &ApiSettings::startedChanged);
    connect(Api::instance().get(), &Api::stopped, this, &ApiSettings::startedChanged);
    connect(Api::instance().get(), &Api::connected, this, &ApiSettings::connectedChanged);
}

ApiSettings::~ApiSettings() = default;

QStringList ApiSettings::servers() const
{
    return Api::instance()->servers();
}

void ApiSettings::setServers(const QStringList & newServers)
{
    if (newServers == Api::instance()->servers()) {
        return;
    }

    Api::instance()->setServers(newServers);
    Q_EMIT serversChanged();
}

QString ApiSettings::connectedServer() const
{
    return Api::instance()->connectedServer();
}

QString ApiSettings::path() const
{
    return Api::instance()->apiPath();
}

void ApiSettings::setPath(const QString & newPath)
{
    if (newPath == Api::instance()->apiPath()) {
        return;
    }

    Api::instance()->setApiPath(newPath);
    Q_EMIT pathChanged();
}

QString ApiSettings::userName() const
{
    return Api::instance()->userName();
}

void ApiSettings::setUserName(const QString& newUserName)
{
    if (newUserName == Api::instance()->userName()) {
        return;
    }

    Api::instance()->setUserName(newUserName);
    Q_EMIT userNameChanged();
}

QString ApiSettings::password() const
{
    return Api::instance()->password();
}

void ApiSettings::setPassword(const QString & newPassword)
{
    if (newPassword == Api::instance()->password()) {
        return;
    }

    Api::instance()->setPassword(newPassword);
    Q_EMIT passwordChanged();
}

bool ApiSettings::started() const
{
    return Api::instance()->isStarted();
}

void ApiSettings::setStarted(bool newStarted)
{
    if (newStarted == Api::instance()->isStarted()) {
        return;
    }

    if (newStarted) {
        Api::instance()->start();
    } else {
        Api::instance()->stop();
    }

    Q_EMIT startedChanged();
}

bool Zax::JsonApi::ApiSettings::connected() const
{
    return Api::instance()->isConnected();
}
