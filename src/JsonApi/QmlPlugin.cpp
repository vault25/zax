/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "QmlPlugin.h"

#include <QQmlEngine>

#include "Api.h"
#include "ApiModel.h"
// #include "ImageProvider.h"
#include "Query.h"
#include "FilterRule.h"
#include "SortRule.h"
#include "IncludeRule.h"
#include "ClientFilterRule.h"

using namespace Zax::JsonApi;

void QmlPlugin::registerTypes(const char* uri)
{
    Q_ASSERT(QLatin1String(uri) == QLatin1String("Zax.JsonApi"));

    qmlRegisterType<ApiModel>(uri, 1, 0, "ApiModel");
    qmlRegisterSingletonInstance(uri, 1, 0, "JsonApi", Api::instance().get());
    qmlRegisterType<Query>(uri, 1, 0, "Query");

    qmlRegisterUncreatableType<QueryRule>(uri, 1, 0, "QueryRule", QStringLiteral("Abstract base class"));
    qmlRegisterType<FilterRule>(uri, 1, 0, "FilterRule");
    qmlRegisterType<SortRule>(uri, 1, 0, "SortRule");
    qmlRegisterType<IncludeRule>(uri, 1, 0, "IncludeRule");
    qmlRegisterType<ClientFilterRule>(uri, 1, 0, "ClientFilterRule");
}

void QmlPlugin::initializeEngine(QQmlEngine* engine, const char* uri)
{
    Q_ASSERT(QLatin1String(uri) == QLatin1String("Zax.JsonApi"));

    // engine->addImageProvider(QStringLiteral("jsonapi"), new JsonApi::ImageProvider{});
}
