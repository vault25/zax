// SPDX-FileCopyrightText: 2023 Arjen Hiemstra <ahiemstra@heimr.nl>
// 
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include <QObject>
#include <qqmlregistration.h>

#include "jsonapi_export.h"

namespace Zax {
namespace JsonApi {

class ApiResult;

class JSONAPI_EXPORT Request : public QObject
{
    Q_OBJECT
    QML_ELEMENT

public:
    enum Method {
        Post,
        Patch,
        Delete
    };
    Q_ENUM(Method);

    Request(QObject* parent = nullptr);
    ~Request() override;

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(Method method READ method WRITE setMethod NOTIFY methodChanged)
    Method method() const;
    void setMethod(Method newMethod);
    Q_SIGNAL void methodChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    QString path() const;
    void setPath(const QString& newPath);
    Q_SIGNAL void pathChanged();

    Q_PROPERTY(QStringList paths READ paths WRITE setPaths NOTIFY pathsChanged)
    QStringList paths() const;
    void setPaths(const QStringList& newPaths);
    Q_SIGNAL void pathsChanged();

    /**
     * TODO: Documentation
     */
    Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)
    QString type() const;
    void setType(const QString& newType);
    Q_SIGNAL void typeChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QVariantMap attributes READ attributes WRITE setAttributes NOTIFY attributesChanged)
    QVariantMap attributes() const;
    void setAttributes(const QVariantMap& newAttributes);
    Q_SIGNAL void attributesChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QVariant data READ data WRITE setData NOTIFY dataChanged)
    QVariant data() const;
    void setData(const QVariant& newData);
    Q_SIGNAL void dataChanged();

    Q_PROPERTY(QString errorString READ errorString NOTIFY errorStringChanged)
    QString errorString() const;
    Q_SIGNAL void errorStringChanged();

    Q_INVOKABLE void execute();

    Q_SIGNAL void success();
    Q_SIGNAL void error();

    ApiResult* result() const;

private:
    void onSuccess(const ApiResult* result);
    void onError(const ApiResult* result);

    class Private;
    const std::unique_ptr<Private> d;
};

}
}
