/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <functional>
#include <memory>

#include <QAbstractListModel>
#include <QQmlListProperty>
#include <QQmlParserStatus>
#include <QVariant>
#include <qqmlregistration.h>

#include "Document.h"
#include "Query.h"
#include "QueryRule.h"

#include "jsonapi_export.h"

namespace Zax {
namespace JsonApi {

    /**
     * A model to represent items provided by JSON:API.
     */
    class JSONAPI_EXPORT ApiModel : public QAbstractListModel, public QQmlParserStatus {
        Q_OBJECT
        QML_ELEMENT
        Q_INTERFACES(QQmlParserStatus)
        Q_CLASSINFO("DefaultProperty", "rules")

    public:
        enum Roles {
            IdRole = Qt::UserRole + 1,
            TypeRole,
            DocumentRole,
        };
        Q_ENUM(Roles)

        enum SortDirection {
            SortAscending,
            SortDescending,
        };
        Q_ENUM(SortDirection)

        ApiModel(QObject* parent = nullptr);
        ~ApiModel() override;

        Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
        QString path() const;
        void setPath(const QString& path);
        Q_SIGNAL void pathChanged();

        Q_PROPERTY(QVariantMap attributes READ attributes WRITE setAttributes NOTIFY attributesChanged)
        QVariantMap attributes() const;
        void setAttributes(const QVariantMap& newAttributes);
        Q_SIGNAL void attributesChanged();

        Q_PROPERTY(QVariantMap relationships READ relationships WRITE setRelationships NOTIFY relationshipsChanged)
        QVariantMap relationships() const;
        void setRelationships(const QVariantMap& newRelationships);
        Q_SIGNAL void relationshipsChanged();

        Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
        int rowCount(const QModelIndex& parent = QModelIndex {}) const override;
        Q_SIGNAL void countChanged();

        Q_PROPERTY(Zax::JsonApi::Query* query READ query CONSTANT)
        Query* query() const;

        Q_PROPERTY(QQmlListProperty<Zax::JsonApi::QueryRule> rules READ rules NOTIFY rulesChanged)
        QQmlListProperty<Zax::JsonApi::QueryRule> rules();
        Q_SIGNAL void rulesChanged();

        Q_INVOKABLE Document get(int index) const;
        Q_INVOKABLE void refresh();

        QHash<int, QByteArray> roleNames() const override;
        QVariant data(const QModelIndex& index, int role) const override;

        //     bool canFetchMore(const QModelIndex& parent) const override;
        //     void fetchMore(const QModelIndex& parent) override;

        void classBegin() override;
        void componentComplete() override;

    private:
        void onSuccess();
        void clearAndRefresh();

        class Private;
        const std::unique_ptr<Private> d;
    };

} // namespace JsonApi
} // namespace Zax
