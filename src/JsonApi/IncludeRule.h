/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <QVariant>
#include "QueryRule.h"

#include "jsonapi_export.h"

namespace Zax {
namespace JsonApi {

/**
 * @todo write docs
 */
class JSONAPI_EXPORT IncludeRule : public QueryRule
{
    Q_OBJECT
    QML_ELEMENT

public:
    IncludeRule(QObject* parent = nullptr);

    void modifyQuery(QUrlQuery& query) override;
    void modifyResult(const QJsonDocument& document, QList<Document>& output) override;

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QString field READ field WRITE setField NOTIFY fieldChanged)
    QString field() const;
    void setField(const QString & newField);
    Q_SIGNAL void fieldChanged();

private:
    QString m_field;
};

}
}
