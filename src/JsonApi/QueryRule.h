/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <QObject>
#include <qqmlregistration.h>

#include "jsonapi_export.h"

class QUrlQuery;

namespace Zax {
namespace JsonApi {

class Document;

/**
 * @todo write docs
 */
class JSONAPI_EXPORT QueryRule : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("Abstract base class")

public:
    QueryRule(QObject* parent = nullptr);
    virtual ~QueryRule();

    virtual void modifyQuery(QUrlQuery& query) = 0;
    virtual void modifyResult(const QJsonDocument& document, QList<Document>& output) = 0;

    Q_SIGNAL void changed();

};

}
}
