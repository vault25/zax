// SPDX-FileCopyrightText: 2023 Arjen Hiemstra <ahiemstra@heimr.nl>
// 
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "Document.h"

#include <QJsonArray>

#include "jsonapi_logging.h"

namespace Zax {
namespace JsonApi {

using namespace Qt::StringLiterals;

struct DefaultAttributeTypeInitializer
{
    template <int typeId>
    static QJsonValue jsonFromVariant(const QVariant& input)
    {
        return input.typeId() == typeId ? QJsonValue::fromVariant(input) : QJsonValue::Undefined;
    }

    DefaultAttributeTypeInitializer()
    {
        Document::registerAttributeType(
            u"bool",
            [](const QJsonValue& input) -> QVariant {
                return input.isBool() ? input.toVariant() : QVariant{};
            },
            jsonFromVariant<QMetaType::Bool>
        );
        Document::registerAttributeType(
            u"int",
            [](const QJsonValue& input) -> QVariant {
                return input.isDouble() ? input.toInt() : QVariant{};
            },
            [](const QVariant& input) -> QJsonValue {
                if (input.typeId() == QMetaType::Int || input.typeId() == QMetaType::Double) {
                    return QJsonValue::fromVariant(input);
                }
                return QJsonValue::Undefined;
            }
        );
        Document::registerAttributeType(
            u"float",
            [](const QJsonValue& input) -> QVariant {
                return input.isDouble() ? input.toDouble() : QVariant{};
            },
            jsonFromVariant<QMetaType::Double>
        );
        Document::registerAttributeType(
            u"string",
            [](const QJsonValue& input) -> QVariant {
                if (input.isObject() && input.toObject().contains(u"value")) {
                    return input.toObject().value(u"value").toString();
                } else if (input.isString()) {
                    return input.toString();
                }
                return QVariant{};
            },
            jsonFromVariant<QMetaType::QString>
        );
        Document::registerAttributeType(
            u"date",
            [](const QJsonValue& input) -> QVariant {
                if (input.isString()) {
                    return QDateTime::fromString(input.toString(), Qt::ISODate);
                }
                return QVariant{};
            },
            [](const QVariant& value) -> QJsonValue {
                return value.value<QDateTime>().toString(Qt::ISODate);
            }
        );
        Document::registerAttributeType(
            u"url",
            [](const QJsonValue& input) -> QVariant {
                if (input.isObject()) {
                    return QUrl{input.toObject().value(u"uri").toString()};
                } else if (input.isString()) {
                    return QUrl{input.toString()};
                }
                return QVariant{};
            },
            jsonFromVariant<QMetaType::QUrl>
        );
        Document::registerAttributeType(
            u"object",
            [](const QJsonValue& input) -> QVariant {
                return input.isObject() ? input.toObject().toVariantMap() : QVariant{};
            },
            [](const QVariant& input) -> QJsonValue {
                return input.canConvert<QVariantMap>() ? QJsonValue(QJsonObject::fromVariantMap(input.toMap())) : QJsonValue::Undefined;
            }
        );
        Document::registerAttributeType(
            u"variant",
            [](const QJsonValue& input) -> QVariant {
                return input.toVariant();
            },
            [](const QVariant& input) -> QJsonValue {
                return QJsonValue::fromVariant(input);
            }
        );
    }
};
static DefaultAttributeTypeInitializer initializer;

QHash<QString, std::pair<Document::FromJsonFunction, Document::ToJsonFunction>> Document::s_attributeTypes;

QJsonObject relationshipObject(const QString& type, const QVariant& value)
{
    auto object = QJsonObject {};

    const auto list = value.toList();
    if (list.size() > 1) {
        auto array = QJsonArray {};
        for (auto id : list) {
            array.append(QJsonObject { { u"type"_s, type }, { u"id"_s, id.toString() } });
        }
        object[u"data"] = array;
    } else {
        object[u"data"] = QJsonObject { { u"type"_s, type }, { u"id"_s, value.toString() } };
    }

    return object;
}

Document::Document()
{
}

bool Document::isEmpty()
{
    return m_id.isEmpty() && m_type.isEmpty() && m_attributes.isEmpty();
}

QString Document::id() const
{
    return m_id;
}

void Document::setId(const QString& newId)
{
    m_id = newId;
}

QString Document::type() const
{
    return m_type;
}

void Document::setType(const QString& newType)
{
    m_type = newType;
}

QVariantMap Document::attributes() const
{
    return m_attributes;
}

void Document::setAttributes(const QVariantMap& newAttributes)
{
    m_attributes = newAttributes;
}

QVariantMap Document::attributeValues() const
{
    return m_attributeValues;
}

void Document::setAttributeValues(const QVariantMap& data)
{
    for (auto itr = data.keyValueBegin(); itr != data.keyValueEnd(); ++itr) {
        if (m_attributes.contains(itr->first)) {
            m_attributeValues[itr->first] = itr->second;
        }
    }
}

void Document::setAttributeValues(const QJsonObject& data)
{
    for (auto itr = data.begin(); itr != data.end(); ++itr) {
        if (m_attributes.contains(itr.key())) {
            m_attributeValues[itr.key()] = valueFromJson(itr.key(), itr.value());
        }
    }
}

QVariant Document::attributeValue(QStringView attribute)
{
    return m_attributeValues.value(attribute.toString());
}

void Document::setAttributeValue(const QString& attribute, const QVariant& value)
{
    if (!m_attributes.contains(attribute)) {
        qCDebug(JSONAPI) << "Igoring value for unknown attribute" << attribute;
        return;
    }

    m_attributeValues[attribute] = value;
}

void Document::setAttributeValue(const QString& attribute, const QJsonValue& value)
{
    if (!m_attributes.contains(attribute)) {
        qCDebug(JSONAPI) << "Igoring value for unknown attribute" << attribute;
        return;
    }
    m_attributeValues[attribute] = valueFromJson(attribute, value);
}

QVariantMap Document::relationships() const
{
    return m_relationships;
}

void Document::setRelationships(const QVariantMap& newRelationships)
{
    m_relationships = newRelationships;
}

QVariantMap Document::relationshipValues()
{
    return m_relationshipValues;
}

void Document::setRelationshipValue(const QString& relationship, const QVariant& value)
{
    if (!m_relationships.contains(relationship)) {
        qCDebug(JSONAPI) << "Igoring value for unknown relationship" << relationship;
        return;
    }

    m_relationshipValues[relationship] = value;
}

QJsonObject Document::toJsonObject()
{
    if (m_type.isEmpty()) {
        qCWarning(JSONAPI) << "JSON:API documents require a type!";
        return QJsonObject{};
    }

    QJsonObject result;
    result[u"type"] = m_type;

    if (!m_id.isEmpty()) {
        result[u"id"] = m_id;
    }

    QJsonObject attributes;
    const auto keys = m_attributes.keys();
    for (auto attribute : keys) {
        if (m_relationships.contains(attribute)) {
            continue;
        }
        attributes[attribute] = valueToJson(attribute);
    }
    result[u"attributes"] = attributes;

    QJsonObject relationships;
    for (auto [relationship, value] : m_relationshipValues.asKeyValueRange()) {
        if (!m_relationships.contains(relationship)) {
            qCDebug(JSONAPI) << "Ignoring unknown relationship" << relationship;
            continue;
        }

        auto relationshipType = m_relationships.value(relationship).toString();
        relationships[relationship] = relationshipObject(relationshipType, value);
    }
    result[u"relationships"] = relationships;

    return result;
}

Document Document::fromJsonObject(const QJsonObject& object, const QVariantMap& attributes, const QVariantMap& relationships)
{
    if (!object.contains(u"id") || !object.contains(u"type") || !object.contains(u"attributes")) {
        qCDebug(JSONAPI) << "Tried to create a Document from an invalid JSON object";
        return Document{};
    }

    Document document;
    document.setId(object.value(u"id").toString());
    document.setType(object.value(u"type").toString());

    document.setAttributes(attributes);
    document.setAttributeValues(object.value(u"attributes").toObject());

    document.setRelationships(relationships);
    if (object.contains(u"relationships")) {
        const auto relationshipObject = object.value(u"relationships").toObject();
        const auto keys = relationshipObject.keys();
        for (auto key : keys) {
            if (!relationships.contains(key)) {
                continue;
            }

            const auto relationshipType = relationships[key].toString();
            const auto relationship = relationshipObject[key];

            if (relationship[u"data"].isArray()) {
                auto data = relationship[u"data"].toArray();
                auto values = QVariantList{};
                for (auto entry : data) {
                    auto object = entry.toObject();
                    if (object[u"type"] == relationshipType) {
                        values.append(object[u"id"].toString());
                    }
                }
                document.setRelationshipValue(key, values);
            } else {
                auto data = relationship[u"data"].toObject();
                if (data[u"type"] == relationshipType) {
                    document.setRelationshipValue(key, data.value(u"id").toString());
                }
            }
        }
    }

    return document;
}

Document Document::fromJsonObject(const QJsonObject& object, const Document& docTemplate)
{
    return fromJsonObject(object, docTemplate.attributes(), docTemplate.relationships());
}

QVariant Document::valueFromJson(const QString& attribute, const QJsonValue& value) const
{
    if (!m_attributes.contains(attribute)) {
        return QVariant{};
    }

    auto type = m_attributes.value(attribute).toString();
    if (!s_attributeTypes.contains(type)) {
        qCWarning(JSONAPI) << "Unknown type" << type << "for attribute" << attribute << "of object" << m_id;
        return value.toVariant();
    }

    auto function = s_attributeTypes.value(type).first;
    return function(value);
}

QJsonValue Document::valueToJson(const QString& attribute) const
{
    if (!m_attributes.contains(attribute) || !m_attributeValues.contains(attribute)) {
        return QJsonValue{};
    }

    auto type = m_attributes.value(attribute).toString();
    auto value = m_attributeValues.value(attribute);
    if (!s_attributeTypes.contains(type)) {
        qCWarning(JSONAPI) << "Unknown type" << type << "for attribute" << attribute << "of object" << m_id;
        return QJsonValue::fromVariant(value);
    }

    auto function = s_attributeTypes.value(type).second;
    return function(value);
}

void Document::registerAttributeType(QStringView type, const Document::FromJsonFunction& fromJsonFunction, const Document::ToJsonFunction& toJsonFunction)
{
    if (s_attributeTypes.contains(type.toString())) {
        qCWarning(JSONAPI) << "Overwriting already registered attribute type" << type;
    }
    s_attributeTypes[type.toString()] = std::make_pair(fromJsonFunction, toJsonFunction);
}

}
}

QDebug operator<<(QDebug debug, const Zax::JsonApi::Document& document)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << "Document(" << &document << ", id=";
    debug.nospace() << document.id() << ", type=";
    debug.nospace() << document.type() << ", attributes=";
    debug.nospace() << document.attributes() << ", values=";
    debug.nospace() << document.attributeValues();
    debug << ")";
    return debug;
}
