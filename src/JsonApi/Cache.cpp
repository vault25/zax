/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "Cache.h"

#include <chrono>
#include <filesystem>
#include <fstream>

#include <QUrl>
#include <QRunnable>
#include <QThreadPool>
#include <QStandardPaths>
#include <QCryptographicHash>
#include <QDateTime>
#include <QCborMap>
#include <QCborValue>
#include <QScopeGuard>
#include <QPointer>

#include "jsonapi_logging.h"

using namespace Zax::JsonApi;

namespace fs = std::filesystem;

QString cacheId(const QUrl& url)
{
    return url.toString(QUrl::RemoveAuthority | QUrl::RemoveScheme);
}

struct Zax::JsonApi::WriteRunner : public QRunnable
{
    QString id;
    fs::path path;
    QByteArray data;

    void run() override
    {
        QCborMap cbor;
        cbor.insert(QStringLiteral("id"), id);
        cbor.insert(QStringLiteral("data"), data);
        cbor.insert(QStringLiteral("check"), qChecksum(data));

        auto data = qCompress(cbor.toCborValue().toCbor(), 9);

        std::ofstream stream(path, std::ios::binary);
        if (!stream) {
            qCDebug(JSONAPI) << "Unable to open cache file for writing:" << QByteArray::fromStdString(path);
            return;
        }

        stream << data.toStdString();
    }
};

struct Zax::JsonApi::ReadRunner : public QRunnable
{
    QUrl url;
    QPointer<CacheReply> reply;
    fs::path path;

    void run() override
    {
        auto guard = qScopeGuard([this]() {
            if (reply) {
                Q_EMIT reply->finished();
            }
            if (fs::exists(path)) {
                fs::remove(path);
            }
        });

        std::ifstream stream(path, std::ios::binary);
        if (!stream) {
            qCDebug(JSONAPI) << "Unable to open cache file for reading" << QByteArray::fromStdString(path);
            return;
        }

        auto fileData = std::string(std::istreambuf_iterator<char>{stream}, {});
        stream.close();

        auto uncompressed = qUncompress(QByteArray::fromStdString(fileData));

        auto cbor = QCborValue::fromCbor(uncompressed).toMap();
        if (cbor.isEmpty()) {
            qCDebug(JSONAPI) << "Got empty CBOR data from cache file" << QByteArray::fromStdString(path);
            return;
        }

        auto id = cbor.value(QStringLiteral("id")).toString();
        auto data = cbor.value(QStringLiteral("data")).toByteArray();
        auto check = quint16(cbor.value(QStringLiteral("check")).toInteger());

        if (id != cacheId(url)) {
            qCDebug(JSONAPI) << "Cache ID mismatch for cache file" << QByteArray::fromStdString(path);
            return;
        }

        if (qChecksum(data) != check) {
            qCDebug(JSONAPI) << "Data checksum mismatch for cache file" << QByteArray::fromStdString(path);
            return;
        }

        guard.dismiss();

        if (!reply) {
            return;
        }

        reply->setData(data);
    }
};

CacheReply::CacheReply(const QUrl& url)
    : QObject()
    , m_url(url)
{
}

QUrl CacheReply::url() const
{
    return m_url;
}

QByteArray CacheReply::data() const
{
    if (!m_initialized) {
        return QByteArray();
    }

    return m_data;
}

bool CacheReply::isValid()
{
    if (!m_initialized) {
        return false;
    }

    return !m_data.isEmpty();
}

void CacheReply::setData(const QByteArray& data)
{
    m_data = data;
    m_initialized = true;

    // Ensure we give client code a moment to setup connections to relevant
    // signals before we emit our finished signal. Otherwise we risk emitting
    // before client code can connect, making client code a lot harder to write
    // since it would need to check finished status next to connecting to
    // signals.
    QMetaObject::invokeMethod(this, [this]() {
        Q_EMIT finished();
    }, Qt::QueuedConnection);
}

class JSONAPI_NO_EXPORT Cache::Private
{
public:
    fs::path cacheFilePath(const QUrl& url);

    fs::file_time_type::duration lifeTime = std::chrono::minutes(10);

    QThreadPool* threadPool;
};


Cache::Cache(QObject* parent)
    : QObject(parent)
    , d(new Private)
{
    d->threadPool = new QThreadPool{this};
    d->threadPool->setMaxThreadCount(1);
}

Cache::~Cache() = default;

bool Cache::isValidFor(const QUrl& url)
{
    static bool noCache = qEnvironmentVariableIsSet("ZAX_JSONAPI_DISABLE_CACHE");
    if (noCache) {
        return false;
    }

    auto path = d->cacheFilePath(url);

    if (!fs::exists(path)) {
        return false;
    }

    auto timeStamp = fs::last_write_time(path);
    if (fs::file_time_type::clock::now() - timeStamp > d->lifeTime) {
        return false;
    }

    return true;
}

std::unique_ptr<CacheReply> Cache::get(const QUrl& url)
{
    auto reply = std::make_unique<CacheReply>(url);

    auto runnable = new ReadRunner{};
    runnable->url = url;
    runnable->path = d->cacheFilePath(url);
    runnable->reply = reply.get();

    d->threadPool->start(runnable);

    return reply;
}

void Cache::cache(const QUrl& url, const QByteArray& data)
{
    auto runnable = new WriteRunner{};
    runnable->id = cacheId(url);
    runnable->path = d->cacheFilePath(url);
    runnable->data = data;
    d->threadPool->start(runnable);
}

int Cache::lifeTime() const
{
    return d->lifeTime.count();
}

void Cache::setLifeTime(int newLifeTime)
{
    setLifeTime(std::chrono::seconds(newLifeTime));
}

void Cache::setLifeTime(std::chrono::seconds newLifeTime)
{
    if (newLifeTime == d->lifeTime) {
        return;
    }

    d->lifeTime = newLifeTime;
    Q_EMIT lifeTimeChanged();
}

void Cache::remove(const QUrl& url)
{
    auto filePath = d->cacheFilePath(url);
    if (fs::exists(filePath)) {
        fs::remove(filePath);
    }
}

void Cache::clear()
{
    fs::remove_all(fs::path{QStandardPaths::writableLocation(QStandardPaths::CacheLocation).toStdString()} / "jsonapi");
}

void Cache::waitForFinished()
{
    d->threadPool->waitForDone();
}

fs::path Cache::Private::cacheFilePath(const QUrl& url)
{
    auto id = cacheId(url);

    static auto storagePath = fs::path{QStandardPaths::writableLocation(QStandardPaths::CacheLocation).toStdString()} / "jsonapi";

    fs::create_directories(storagePath);

    // Note that MD5 is used here because it seems to be the fastest of the
    // algorithms implemented by QCryptographicHash. Since we are not using this
    // for security, merely as an identifier, the security of the algorithm
    // shouldn't matter much anyway.
    auto fileName = QCryptographicHash::hash(id.toUtf8(), QCryptographicHash::Md5).toHex().toStdString();

    return storagePath / fileName;
}
