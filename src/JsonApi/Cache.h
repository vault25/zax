/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <optional>
#include <chrono>

#include <QObject>
#include <QUrl>
#include <QMutex>

#include "jsonapi_export.h"

namespace Zax {
namespace JsonApi {

struct WriteRunner;
struct ReadRunner;

class CacheReply : public QObject
{
    Q_OBJECT
public:
    CacheReply(const QUrl& url);

    QUrl url() const;
    QByteArray data() const;
    bool isValid();

    Q_SIGNAL void finished();

private:
    friend ReadRunner;
    void setData(const QByteArray& data);

    QUrl m_url;
    QByteArray m_data;
    std::atomic_bool m_initialized = false;
};

class JSONAPI_EXPORT Cache : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int lifeTime READ lifeTime WRITE setLifeTime NOTIFY lifeTimeChanged)

public:
    enum class CacheMode {
        Cache,
        DoNotCache,
        CacheOnly,
        ForceRefresh,
    };
    Q_ENUM(CacheMode)

    Cache(QObject* parent = nullptr);
    ~Cache() override;

    bool isValidFor(const QUrl& url);

    void cache(const QUrl& url, const QByteArray& data);

    std::unique_ptr<CacheReply> get(const QUrl& url);

    int lifeTime() const;
    void setLifeTime(int newLifeTime);
    void setLifeTime(std::chrono::seconds newLifeTime);
    Q_SIGNAL void lifeTimeChanged();

    Q_INVOKABLE void remove(const QUrl& url);

    Q_INVOKABLE void clear();

    void waitForFinished();

private:
    class Private;
    const std::unique_ptr<Private> d;
};

} // namespace JsonApi
} // namespace Zax
