/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "FilterRule.h"

#include <QUrlQuery>

using namespace Zax::JsonApi;

FilterRule::FilterRule(QObject* parent)
    : QueryRule(parent)
{
}

void FilterRule::modifyQuery(QUrlQuery& query)
{
    query.addQueryItem(QStringLiteral("filter[%1][value]").arg(m_field), m_value.toString());
}

void FilterRule::modifyResult(const QJsonDocument& document, QList<Document>& output)
{
    Q_UNUSED(document);
    Q_UNUSED(output);
}


QString FilterRule::field() const
{
    return m_field;
}

void FilterRule::setField(const QString& newField)
{
    if (newField == m_field) {
        return;
    }

    m_field = newField;
    Q_EMIT fieldChanged();
    Q_EMIT changed();
}

QVariant FilterRule::value() const
{
    return m_value;
}

void FilterRule::setValue(const QVariant& newValue)
{
    if (newValue == m_value) {
        return;
    }

    m_value = newValue;
    Q_EMIT valueChanged();
    Q_EMIT changed();
}
