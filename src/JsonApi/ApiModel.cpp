/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "ApiModel.h"

#include <QDateTime>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QScopeGuard>
#include <QUrl>
#include <vector>

#include "Api.h"
#include "ApiResult.h"
#include "Document.h"
#include "Query.h"

#include "jsonapi_logging.h"

using namespace Qt::StringLiterals;

using namespace Zax::JsonApi;

class JSONAPI_NO_EXPORT ApiModel::Private {
public:
    std::unique_ptr<Query> query;

    QVariantMap attributes;
    QVariantMap relationships;
    QHash<int, QByteArray> roleNames;

    bool completed = false;

    int limit = std::numeric_limits<int>::max();
    int page = 0;

    QList<Document> items;

    static const QHash<int, QByteArray> fixedRoles;
};

const QHash<int, QByteArray> ApiModel::Private::fixedRoles = {
    { IdRole, "itemId" },
    { TypeRole, "type" },
    { DocumentRole, "document" },
};

ApiModel::ApiModel(QObject* parent)
    : QAbstractListModel(parent)
    , d(new Private)
{
    d->query = std::make_unique<Query>();
    connect(d->query.get(), &Query::pathChanged, this, &ApiModel::pathChanged);
    connect(d->query.get(), &Query::rulesChanged, this, &ApiModel::rulesChanged);
    connect(d->query.get(), &Query::changed, this, &ApiModel::refresh);
    connect(d->query.get(), &Query::success, this, &ApiModel::onSuccess);

    connect(d->query.get(), &Query::attributesChanged, this, [this]() {
        d->roleNames.clear();
        d->roleNames.insert(Private::fixedRoles);

        const auto attributes = d->query->attributes();
        auto nextRole = DocumentRole + 1;
        for (auto itr = attributes.keyBegin(); itr != attributes.keyEnd(); ++itr) {
            d->roleNames.insert(nextRole++, (*itr).toUtf8());
        }

        clearAndRefresh();
        Q_EMIT attributesChanged();
    });
    connect(d->query.get(), &Query::relationshipsChanged, this, [this]() {
        clearAndRefresh();
        Q_EMIT relationshipsChanged();
    });
}

ApiModel::~ApiModel() = default;

QHash<int, QByteArray> ApiModel::roleNames() const
{
    return d->roleNames.isEmpty() ? Private::fixedRoles : d->roleNames;
}

QString ApiModel::path() const
{
    return d->query->path();
}

void ApiModel::setPath(const QString& path)
{
    d->query->setPath(path);
}

Query* ApiModel::query() const
{
    return d->query.get();
}

QVariantMap ApiModel::attributes() const
{
    return d->query->attributes();
}

void ApiModel::setAttributes(const QVariantMap& newAttributes)
{
    d->query->setAttributes(newAttributes);
}

QVariantMap Zax::JsonApi::ApiModel::relationships() const
{
    return d->query->relationships();
}

void ApiModel::setRelationships(const QVariantMap& newRelationships)
{
    d->query->setRelationships(newRelationships);
}

QQmlListProperty<QueryRule> ApiModel::rules()
{
    return d->query->rulesProperty();
}

Document ApiModel::get(int index) const
{
    if (index >= 0 && index < d->items.size()) {
        return d->items.at(std::size_t(index));
    }

    // If the entry is not found, we want to return an empty document that still
    // contains all the relevant attributes so QML code doesn't need to check
    // if each attribute exists.
    auto empty = Document {};
    empty.setId(u"empty"_s);
    empty.setAttributes(d->attributes);

    auto emptyAttributes = d->attributes;
    std::fill(emptyAttributes.begin(), emptyAttributes.end(), QString {});
    empty.setAttributeValues(emptyAttributes);

    return empty;
}

void ApiModel::refresh()
{
    if (!d->completed) {
        return;
    }

    d->query->refresh();
}

int ApiModel::rowCount(const QModelIndex& parent) const
{
    if (!parent.isValid()) {
        return std::min(d->limit, int(d->items.size()));
    }

    return 0;
}

QVariant ApiModel::data(const QModelIndex& index, int role) const
{
    if (!checkIndex(index, CheckIndexOption::IndexIsValid | CheckIndexOption::DoNotUseParent)) {
        return QVariant {};
    }

    auto item = d->items.at(index.row());

    if (role == IdRole) {
        return item.id();
    } else if (role == TypeRole) {
        return item.type();
    } else if (role == DocumentRole) {
        return QVariant::fromValue(get(index.row()));
    } else if (d->roleNames.contains(role)) {
        return item.attributeValue(QString::fromUtf8(d->roleNames.value(role)));
    }

    return QVariant {};
}

void ApiModel::classBegin()
{
    d->query->classBegin();
}

void ApiModel::componentComplete()
{
    d->completed = true;
    d->query->componentComplete();
    refresh();
}

void ApiModel::onSuccess()
{
    const auto entries = d->query->entries();

    if (entries.size() != qsizetype(d->items.size())) {
        beginResetModel();
        d->items = entries;
        endResetModel();
    } else {
        for (int i = 0; i < entries.size(); ++i) {
            d->items[i] = entries.at(i);
        }
        Q_EMIT dataChanged(index(0, 0), index(entries.size() - 1, 0));
    }

    Q_EMIT countChanged();
}

void ApiModel::clearAndRefresh()
{
    if (d->completed) {
        beginResetModel();
        d->items.clear();
        endResetModel();
    }

    refresh();
}
