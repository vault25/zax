#include "Api.h"

#include <QStandardPaths>
#include <QDir>
#include <QCryptographicHash>
#include <QUrl>
#include <QTimer>
#include <QStringBuilder>
#include <QThread>

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QSslError>
#include <QtNetwork/QAuthenticator>

#include <QDateTime>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include "Cache.h"

#include "jsonapi_logging.h"

using namespace Zax::JsonApi;

namespace fs = std::filesystem;

static const auto JsonApiMime = QByteArrayLiteral("application/vnd.api+json");

QVariantMap convertRelation(const QJsonObject& input)
{
    auto id = input.value(u"id");
    if (id.isNull() || id.isUndefined()) {
        return QVariantMap{};
    }

    return QVariantMap{
        {QStringLiteral("id"), id},
        {QStringLiteral("type"), QStringLiteral("relationship")}
    };
}

class JSONAPI_NO_EXPORT Api::Private
{
public:
    Private(Api* qq)
        : q(qq)
    {
    }

    void selectServer();
    QNetworkRequest createRequest(const QUrl& url);

    Api* const q;

    std::unique_ptr<QNetworkAccessManager> manager;
    std::unique_ptr<QTimer> serverSelectTimer;
    std::unique_ptr<Cache> cache;

    int selectedServer = -1;
    QStringList servers;
    QSet<QUrl> checkedServers;

    bool started = false;

    QString userName;
    QString password;

    QString apiPath = QStringLiteral("/jsonapi/");
};

Api::Api(QObject* parent)
    : QObject(parent)
    , d(new Private(this))
{
    d->manager = std::make_unique<QNetworkAccessManager>();
    d->manager->setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);
    d->manager->setTransferTimeout(5000);

    d->cache = std::make_unique<Cache>();

    d->serverSelectTimer = std::make_unique<QTimer>();
    connect(d->serverSelectTimer.get(), &QTimer::timeout, this, [this]() {
        d->selectServer();
    });
    d->serverSelectTimer->setInterval(60000);
}

Api::~Api()
{
}

bool Api::isStarted() const
{
    return d->serverSelectTimer->isActive();
}

bool Api::isConnected() const
{
    return d->selectedServer >= 0;
}

std::chrono::milliseconds Api::timeout() const
{
    return std::chrono::milliseconds(d->manager->transferTimeout());
}

void Api::setTimeout(std::chrono::milliseconds newTimeout)
{
    d->manager->setTransferTimeout(newTimeout);
}

ApiResultPtr Api::get(QStringView path, QStringView filters, Cache::CacheMode cache)
{
    QUrl url = buildUrl(path, filters);
    return get(url, cache);
}

ApiResultPtr Api::get(const QUrl& url, Cache::CacheMode cache)
{
    // Unfortunately, QNetworkAccessManager can only be used from a single thread.
    Q_ASSERT(QThread::currentThread() == d->manager->thread());

    if (!d->started) {
        return std::make_unique<ApiResult>();
    }

    if (cache != Cache::CacheMode::DoNotCache) {
        bool validCache = cache != Cache::CacheMode::ForceRefresh && d->cache->isValidFor(url);
        if (d->selectedServer < 0 || validCache) {
            qCDebug(JSONAPI) << "Requesting" << url << "from cache";
            return std::make_unique<ApiResult>(d->cache->get(url));
        }
    }

    if (cache == Cache::CacheMode::CacheOnly) {
        return std::make_unique<ApiResult>(QStringLiteral("CacheOnly requested but URL not found in cache"));
    }

    qCDebug(JSONAPI) << "Requesting" << url << "from remote";
    auto result = std::make_unique<ApiResult>(d->manager->get(d->createRequest(url)));
    result->setCacheMode(cache);
    return result;
}

ApiResultPtr Api::post(QStringView path, const QJsonObject& data, const QString &query)
{
    // Unfortunately, QNetworkAccessManager can only be used from a single thread.
    Q_ASSERT(QThread::currentThread() == d->manager->thread());

    if (!d->started) {
        return std::make_unique<ApiResult>();
    }

    auto url = buildUrl(path, query);

    auto request = d->createRequest(url);
    request.setRawHeader("Content-Type", JsonApiMime);

    auto postData = QJsonDocument{data}.toJson();

    qCDebug(JSONAPI) << "Sending POST request to" << url;

    auto result = std::make_unique<ApiResult>(d->manager->post(request, postData));
    result->setCacheMode(Cache::CacheMode::DoNotCache);
    return result;
}

ApiResultPtr Api::patch(QStringView path, const QJsonObject& data, const QString &query)
{
    // Unfortunately, QNetworkAccessManager can only be used from a single thread.
    Q_ASSERT(QThread::currentThread() == d->manager->thread());

    if (!d->started) {
        return std::make_unique<ApiResult>();
    }

    auto url = buildUrl(path, query);
    auto request = d->createRequest(url);
    request.setRawHeader("Content-Type", JsonApiMime);

    auto postData = QJsonDocument{data}.toJson();

    qCDebug(JSONAPI) << "Sending PATCH request to" << url;

    auto result = std::make_unique<ApiResult>(d->manager->sendCustomRequest(request, "PATCH", postData));
    result->setCacheMode(Cache::CacheMode::DoNotCache);
    return result;
}


ApiResultPtr Api::del(QStringView path, const QJsonObject& data, const QString &query)
{
    // Unfortunately, QNetworkAccessManager can only be used from a single thread.
    Q_ASSERT(QThread::currentThread() == d->manager->thread());

    if (!d->started) {
        return std::make_unique<ApiResult>();
    }

    auto url = buildUrl(path, query);
    auto request = d->createRequest(url);
    request.setRawHeader("Content-Type", JsonApiMime);

    auto postData = QJsonDocument{data}.toJson();

    qCDebug(JSONAPI) << "Sending PATCH request to" << url;

    auto result = std::make_unique<ApiResult>(d->manager->sendCustomRequest(request, "DELETE", postData));
    result->setCacheMode(Cache::CacheMode::DoNotCache);
    return result;
}

Cache* Api::cache() const
{
    return d->cache.get();
}

QStringList Api::servers() const
{
    return d->servers;
}

void Api::setServers(const QStringList& servers)
{
    d->servers = servers;
}

QString Api::connectedServer() const
{
    return d->servers[d->selectedServer];
}

QString Api::apiPath() const
{
    return d->apiPath;
}

void Api::setApiPath(const QString & newApiPath)
{
    d->apiPath = newApiPath;
}

QString Api::userName() const
{
    return d->userName;
}

void Api::setUserName(const QString& userName)
{
    d->userName = userName;
}

QString Zax::JsonApi::Api::password() const
{
    return d->password;
}

void Api::setPassword(const QString& password)
{
    d->password = password;
}

void Api::start()
{
    d->serverSelectTimer->start();
    Q_EMIT started();
    d->selectServer();
}

void Api::stop()
{
    d->serverSelectTimer->stop();
    d->selectedServer = -1;
    d->checkedServers.clear();
    Q_EMIT stopped();
}

std::shared_ptr<Api> Api::instance()
{
    static auto instance = std::make_shared<Api>();
    return instance;
}

QVariantMap Api::jsonApiToVariantMap(const QJsonObject& apiObject)
{
    if (apiObject.isEmpty()) {
        return QVariantMap{};
    }

    auto result = apiObject.value(QStringLiteral("attributes")).toObject().toVariantMap();
    if (result.isEmpty()) {
        return QVariantMap{};
    }

    result.insert(QStringLiteral("id"), apiObject.value(u"id").toString());
    result.insert(QStringLiteral("type"), apiObject.value(u"type").toString());

    const auto relations = apiObject.value(u"relationships").toObject();
    const auto keys = relations.keys();
    for (auto key : keys) {
        auto data = relations.value(key).toObject().value(QStringLiteral("data"));

        if (data.isArray()) {
            auto list = QVariantList{};
            const auto array = data.toArray();

            for (auto entry : array) {
                list.append(convertRelation(entry.toObject()));
            }

            result.insert(key, list);
        } else if (data.isObject()) {
            result.insert(key, convertRelation(data.toObject()));
        }
    }

    return result;
}

bool Api::validateJsonApi(ApiResult* result)
{
    auto document = result->asJson().value_or(QJsonDocument{});
    if(document.isEmpty() || !document.isObject()) {
        result->setErrorString(QStringLiteral("JSON document is empty"));
        return false;
    }

    auto root = document.object();
    if (root.contains(u"jsonapi") && root[u"jsonapi"].toObject()[u"version"].toString() != u"1.0") {
        result->setErrorString(QStringLiteral("JSON API version mismatch; only version 1.0 is supported"));
        return false;
    }

    const auto data = root[u"data"];

    if (!data.isArray() && !data.isObject()) {
        result->setErrorString(QStringLiteral("'data' element is not an array or object"));
        return false;
    }

    return true;
}

QNetworkReply* Api::getFromRemote(const QUrl& url)
{
    if (d->selectedServer < 0) {
        return nullptr;
    }

    return d->manager->get(d->createRequest(url));
}

QUrl Api::buildUrl(QStringView path, QStringView query)
{
    QUrl url;
    if (d->selectedServer >= 0) {
        url = QUrl{d->servers.at(d->selectedServer)};
    }

    // We need to manually clean up duplicate slashes because QUrl apparently
    // can't do that itself.
    auto p = QStringLiteral("%1/%2").arg(d->apiPath, path);
    p = p.replace(QRegularExpression(QStringLiteral("/+")), QStringLiteral("/"));
    url.setPath(p);
    url.setQuery(query.toString());

    // Remove trailing slash, if any remain.
    url = url.adjusted(QUrl::StripTrailingSlash);

    return url;
}

QNetworkRequest Api::Private::createRequest(const QUrl& url)
{
    QNetworkRequest request;
    request.setUrl(url);
    request.setHeader(QNetworkRequest::UserAgentHeader, QStringLiteral("Zax::JsonApi"));
    request.setRawHeader("Accept", JsonApiMime);

    QString credentials = userName + QStringLiteral(":") + password;
    QByteArray encoded = QByteArrayLiteral("Basic ") + credentials.toUtf8().toBase64();
    request.setRawHeader("Authorization", encoded);

    return request;
}

void Api::Private::selectServer()
{
    selectedServer = -1;

    static bool offlineMode = qEnvironmentVariableIsSet("ZAX_JSONAPI_OFFLINE");
    if (offlineMode) {
        qCDebug(JSONAPI) << "ZAX_JSONAPI_OFFLINE is set, simulating offline mode...";
        started = true;
        return;
    }

    for (auto server : servers) {
        auto url = QUrl{server};
        url.setPath(apiPath);
        url = url.adjusted(QUrl::StripTrailingSlash);
        auto reply = manager->get(createRequest(url));
        connect(reply, &QNetworkReply::finished, [this, reply]() {
            if (reply->error() == QNetworkReply::NoError) {
                auto url = reply->url();

                auto index = -1;
                for (int i = 0; i < servers.size(); ++i) {
                    auto serverUrl = QUrl{servers.at(i)};
                    if (url.scheme() == serverUrl.scheme() && url.host() == serverUrl.host()) {
                        index = i;
                        break;
                    }
                }

                if (index >= 0) {
                    qCDebug(JSONAPI) << "Connected to" << url;
                    selectedServer = index;
                    Q_EMIT q->connected();
                }
            } else {
                qCDebug(JSONAPI) << "Unable to connect to" << reply->url() << reply->errorString();
            }
            started = true;
            reply->deleteLater();
        });
    }
}
