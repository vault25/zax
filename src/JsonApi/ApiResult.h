/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <optional>

#include <QObject>
#include <QNetworkReply>

#include "Cache.h"

#include "jsonapi_export.h"

namespace Zax {
namespace JsonApi {

class JSONAPI_EXPORT ApiResult : public QObject
{
    Q_OBJECT

public:
    ApiResult(const QString& errorMessage = QString{});
    ApiResult(QNetworkReply* networkReply);
    ApiResult(std::unique_ptr<CacheReply>&& cacheReply);
    ~ApiResult() override;

    QUrl url() const;
    QByteArray data() const;

    std::optional<QJsonDocument> asJson() const;
    std::optional<QImage> asImage() const;

    QNetworkReply::NetworkError errorCode() const;
    QString errorString() const;
    void setErrorString(const QString& error);

    void setCacheMode(Cache::CacheMode cache);

    void setValidationCallback(const std::function<bool(ApiResult*)>& callback);

    Q_SIGNAL void success(const ApiResult* result);
    Q_SIGNAL void error(const ApiResult* result);

private:
    void emptyReplyFinished();
    void cacheReplyFinished();
    void networkReplyFinished();

    QUrl m_url;
    QByteArray m_data;
    Cache::CacheMode m_cacheMode;

    std::function<bool(ApiResult*)> m_validationCallback;

    QNetworkReply::NetworkError m_errorCode = QNetworkReply::NoError;
    QString m_errorString;
    QNetworkReply* m_networkReply = nullptr;

    std::unique_ptr<CacheReply> m_cacheReply = nullptr;
};

using ApiResultPtr = std::unique_ptr<ApiResult>;

} // namespace JsonApi
} // namespace Zax
