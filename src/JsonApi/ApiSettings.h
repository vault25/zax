// SPDX-FileCopyrightText: 2023 Arjen Hiemstra <ahiemstra@heimr.nl>
// 
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include <QObject>
#include <qqmlregistration.h>

#include "jsonapi_export.h"

namespace Zax {
namespace JsonApi {

class JSONAPI_EXPORT ApiSettings : public QObject
{
    Q_OBJECT
    QML_ELEMENT

public:
    ApiSettings(QObject* parent = nullptr);
    ~ApiSettings() override;

    Q_PROPERTY(QStringList servers READ servers WRITE setServers NOTIFY serversChanged)
    QStringList servers() const;
    void setServers(const QStringList& newServers);
    Q_SIGNAL void serversChanged();

    Q_PROPERTY(QString connectedServer READ connectedServer NOTIFY connectedServerChanged)
    QString connectedServer() const;
    Q_SIGNAL void connectedServerChanged();

    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    QString path() const;
    void setPath(const QString& newPath);
    Q_SIGNAL void pathChanged();

    Q_PROPERTY(QString userName READ userName WRITE setUserName NOTIFY userNameChanged)
    QString userName() const;
    void setUserName(const QString& newUserName);
    Q_SIGNAL void userNameChanged();

    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)
    QString password() const;
    void setPassword(const QString& newPassword);
    Q_SIGNAL void passwordChanged();

    Q_PROPERTY(bool started READ started WRITE setStarted NOTIFY startedChanged)
    bool started() const;
    void setStarted(bool newStarted);
    Q_SIGNAL void startedChanged();

    Q_PROPERTY(bool connected READ connected NOTIFY connectedChanged)
    bool connected() const;
    Q_SIGNAL void connectedChanged();

};

}
}
