/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "ApiResult.h"

#include <QImage>
#include <QJsonDocument>
#include <QNetworkReply>

#include "Api.h"
#include "Cache.h"

#include "jsonapi_logging.h"

using namespace Zax::JsonApi;

ApiResult::ApiResult(const QString& errorMessage)
{
    m_errorCode = QNetworkReply::ServiceUnavailableError;
    m_errorString = errorMessage.isEmpty() ? QStringLiteral("JsonApi is not available") : errorMessage;
    QMetaObject::invokeMethod(this, &ApiResult::emptyReplyFinished, Qt::QueuedConnection);
}

ApiResult::ApiResult(QNetworkReply* networkReply)
{
    m_url = networkReply->url();
    m_networkReply = networkReply;
    connect(m_networkReply, &QNetworkReply::finished, this, &ApiResult::networkReplyFinished);
}

ApiResult::ApiResult(std::unique_ptr<CacheReply>&& cacheReply)
{
    m_cacheReply = std::move(cacheReply);
    m_url = m_cacheReply->url();
    connect(m_cacheReply.get(), &CacheReply::finished, this, &ApiResult::cacheReplyFinished);
}

ApiResult::~ApiResult()
{
    if (m_networkReply) {
        m_networkReply->deleteLater();
    }
}

QUrl ApiResult::url() const
{
    return m_url;
}

QByteArray ApiResult::data() const
{
    return m_data;
}

std::optional<QJsonDocument> ApiResult::asJson() const
{
    if (m_data.isEmpty()) {
        return std::nullopt;
    }

    return QJsonDocument::fromJson(m_data);
}

std::optional<QImage> ApiResult::asImage() const
{
    if (m_data.isEmpty()) {
        return std::nullopt;
    }

    return QImage::fromData(m_data);
}

QNetworkReply::NetworkError ApiResult::errorCode() const
{
    return m_errorCode;
}

QString ApiResult::errorString() const
{
    return m_errorString;
}

void ApiResult::setErrorString(const QString& error)
{
    m_errorString = error;
}

void ApiResult::setCacheMode(Cache::CacheMode cache)
{
    m_cacheMode = cache;
}

void ApiResult::setValidationCallback(const std::function<bool(ApiResult*)>& callback)
{
    m_validationCallback = callback;
}

void ApiResult::emptyReplyFinished()
{
    m_errorCode = QNetworkReply::InternalServerError;
    Q_EMIT error(this);
}

void ApiResult::networkReplyFinished()
{
    auto guard = qScopeGuard([this] {
        m_networkReply->deleteLater();
        m_networkReply = nullptr;
    });

    if (m_networkReply->error() != QNetworkReply::NoError) {
        m_errorCode = m_networkReply->error();
        m_errorString = m_networkReply->errorString();

        if (m_networkReply->isReadable()) {
            m_data = m_networkReply->readAll();
        }

        Q_EMIT error(this);
        return;
    }

    // 204 "No Content" doesn't have content but should be considered a successful request.
    if (m_networkReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 204) {
        Q_EMIT success(this);
        return;
    }

    auto contentType = m_networkReply->header(QNetworkRequest::ContentTypeHeader).toString();
    if (contentType != u"application/vnd.api+json") {
        m_errorCode = QNetworkReply::UnknownContentError;
        m_errorString = u"Invalid content type: %1"_qs.arg(contentType);
    }

    m_data = m_networkReply->readAll();

    if (!m_validationCallback || m_validationCallback(this)) {
        if (m_cacheMode == Cache::CacheMode::Cache || m_cacheMode == Cache::CacheMode::ForceRefresh) {
            Api::instance()->cache()->cache(m_url, m_data);
        }

        Q_EMIT success(this);
        return;
    }

    m_errorCode = QNetworkReply::UnknownContentError;
    if (m_errorString.isEmpty()) {
        m_errorString = QStringLiteral("Content failed validation");
    }
    Q_EMIT error(this);
}

void ApiResult::cacheReplyFinished()
{
    if (m_cacheReply->isValid()) {
        m_data = m_cacheReply->data();

        if (!m_validationCallback || m_validationCallback(this)) {
            Q_EMIT success(this);
            return;
        }
    }

    if (m_cacheMode != Cache::CacheMode::CacheOnly) {
        m_networkReply = Api::instance()->getFromRemote(m_url);
        if (m_networkReply) {
            connect(m_networkReply, &QNetworkReply::finished, this, &ApiResult::networkReplyFinished);
            return;
        }
    }

    m_errorCode = QNetworkReply::UnknownContentError;
    if (m_errorString.isEmpty()) {
        m_errorString = QStringLiteral("Failed getting content from cache");
    }
    Q_EMIT error(this);
}
