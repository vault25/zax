/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "QueryRule.h"

using namespace Zax::JsonApi;

QueryRule::QueryRule(QObject* parent)
    : QObject(parent)
{
}

QueryRule::~QueryRule()
{
}
