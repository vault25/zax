/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "ImageProvider.h"

#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QQuickTextureFactory>
#include <QTimer>
#include <QMetaObject>
#include <QScopeGuard>
#include <QUrlQuery>

#include "Api.h"
#include "ApiResult.h"

#include "jsonapi_logging.h"

using namespace Zax::JsonApi;

ImageProvider::ImageProvider()
{
}

ImageProvider::~ImageProvider()
{
}

QQuickImageResponse* ImageProvider::requestImageResponse(const QString& id, const QSize& requestedSize)
{
    try {
        return new JsonApi::ImageResponse{id, requestedSize};
    } catch (const std::runtime_error& e) {
        qCWarning(JSONAPI) << e.what();
        return nullptr;
    }
}

ImageResponse::ImageResponse(const QString& id, const QSize& requestedSize)
    : m_size(requestedSize)
{
    auto url = QUrl(id);

    auto query = QUrlQuery(url);

    auto field = query.hasQueryItem(QStringLiteral("field")) ?
                 query.queryItemValue(QStringLiteral("field")) :
                 QStringLiteral("field_image");

    m_path = url.path();
    m_filters = QStringLiteral("include=%1&filter[status][value]=1").arg(field);

    if (query.hasQueryItem(QStringLiteral("title"))) {
        m_filters.append(QStringLiteral("filter[title][value]=%1").arg(query.queryItemValue(QStringLiteral("title"))));
    }

    // QNetworkAccessManager only works in the same thread as it was created in.
    // Since the async image provider gets called from a different thread, we
    // need to do some trickery to call across different threads. This
    // invokeMethod call takes care of that, as it pushes the get() call onto
    // the event loop that Api::instance() is running in. The result calls are
    // then done in the event loop of the image provider.
    QMetaObject::invokeMethod(Api::instance().get(), [this]() {
        m_jsonResult = Api::instance()->get(m_path, m_filters);
        m_jsonResult->setValidationCallback(ImageResponse::validateJson);
        connect(m_jsonResult.get(), &ApiResult::success, this, &ImageResponse::jsonSuccess, Qt::QueuedConnection);
        connect(m_jsonResult.get(), &ApiResult::error, this, [this]() {
            Q_EMIT finished();
            m_jsonResult = nullptr;
        }, Qt::QueuedConnection);
    }, Qt::QueuedConnection);
}

QQuickTextureFactory* ImageResponse::textureFactory() const
{
    return QQuickTextureFactory::textureFactoryForImage(m_image);
}

void ImageResponse::jsonSuccess()
{
    auto guard = qScopeGuard([this]() {
        m_jsonResult = nullptr;
    });

    auto imageUrl = m_jsonResult->asJson().value()["included"][0]["attributes"]["uri"]["url"].toVariant().toUrl();

    if (imageUrl.isRelative()) {
        imageUrl = QUrl{Api::instance()->connectedServer() + imageUrl.toString(QUrl::FullyEncoded)};
    }

    // Trickery to deal with multithreading, see above.
    QMetaObject::invokeMethod(Api::instance().get(), [this, imageUrl]() {
        m_imageResult = Api::instance()->get(imageUrl);
        m_imageResult->setValidationCallback([this](ApiResult* result) {
            if (!m_image.loadFromData(result->data())) {
                qCDebug(JSONAPI) << "Invalid data for image" << result->url();
                return false;
            }

            return true;
        });
        connect(m_imageResult.get(), &ApiResult::success, this, &ImageResponse::imageSuccess, Qt::QueuedConnection);
        connect(m_imageResult.get(), &ApiResult::error, this, [this]() {
            Q_EMIT finished();
            m_imageResult = nullptr;
        }, Qt::QueuedConnection);
    }, Qt::QueuedConnection);
}

void ImageResponse::imageSuccess()
{
    if (m_size.isValid()) {
        m_image = m_image.scaled(m_size);
    }

    Q_EMIT finished();
    m_imageResult = nullptr;
}

bool ImageResponse::validateJson(ApiResult* result)
{
    if (!Api::validateJsonApi(result)) {
        return false;
    }

    auto object = result->asJson().value_or(QJsonDocument{}).object();
    if (!object.contains("included")) {
        qCDebug(JSONAPI) << "Missing 'included' entry for image" << result->url();
        return false;
    }

    auto fileInfo = object.value("included").toArray().first().toObject();
    auto imageUrl = fileInfo.value("attributes").toObject().value("uri").toObject().value("url").toVariant().toUrl();
    if (imageUrl.isEmpty()) {
        qCDebug(JSONAPI) << "Image URL is empty for image" << result->url();
        return false;
    }

    return true;
}
