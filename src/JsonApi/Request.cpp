// SPDX-FileCopyrightText: 2023 Arjen Hiemstra <ahiemstra@heimr.nl>
// 
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "Request.h"

#include <ranges>
#include <utility>

#include <QVariantMap>

#include "Api.h"
#include "ApiResult.h"
#include "Document.h"

#include "jsonapi_logging.h"

namespace Zax {
namespace JsonApi {

using namespace Qt::StringLiterals;

class JSONAPI_NO_EXPORT Request::Private
{
public:
    Method method;
    QString path;
    QStringList paths;
    QString type;
    QVariantMap attributes;
    QVariant data;

    std::vector<ApiResultPtr> results;
    QStringList errors;
};

Request::Request(QObject* parent)
    : QObject(parent)
    , d(std::make_unique<Private>())
{
}

Request::~Request() = default;

Request::Method Request::method() const
{
    return d->method;
}

void Request::setMethod(Method newMethod)
{
    if (newMethod == d->method) {
        return;
    }

    d->method = newMethod;
    Q_EMIT methodChanged();
}

QString Request::path() const
{
    return d->path;
}

void Request::setPath(const QString& newPath)
{
    if (newPath == d->path) {
        return;
    }

    d->path = newPath;
    Q_EMIT pathChanged();
}

QStringList Request::paths() const
{
    return d->paths;
}

void Request::setPaths(const QStringList& newPaths)
{
    if (newPaths == d->paths) {
        return;
    }

    d->paths = newPaths;
    Q_EMIT pathsChanged();
}

QString Request::type() const
{
    return d->type;
}

void Request::setType(const QString & newType)
{
    if (newType == d->type) {
        return;
    }

    d->type = newType;
    Q_EMIT typeChanged();
}

QVariantMap Request::attributes() const
{
    return d->attributes;
}

void Request::setAttributes(const QVariantMap& newAttributes)
{
    if (newAttributes == d->attributes) {
        return;
    }

    d->attributes = newAttributes;
    Q_EMIT attributesChanged();
}


QVariant Request::data() const
{
    return d->data;
}

void Request::setData(const QVariant& newData)
{
    if (newData == d->data) {
        return;
    }

    d->data = newData;
    Q_EMIT dataChanged();
}

QString Request::errorString() const
{
    return d->errors.join(u'\n');
}

void Request::execute()
{
    QVariantList data = d->data.value<QVariantList>();
    if (data.isEmpty()) {
        if (d->data.canConvert<Document>()) {
            data.append(d->data);
        }

        if (d->data.canConvert<QVariantMap>()) {
            data.append(d->data);
        }
    }

    if (d->method != Method::Delete && data.isEmpty()) {
        qCWarning(JSONAPI) << "Request data should be either a document, a dictionary of attributes and values or a list of dictionaries";
        return;
    }

    QStringList paths = d->paths;
    if (paths.isEmpty()) {
        paths = {d->path};
    }

    if (d->method == Method::Patch && paths.size() != data.size()) {
        qCWarning(JSONAPI) << "Request paths should match count of data elements";
        return;
    }

    d->errors.clear();

    for (int i = 0; i < data.size(); ++i) {
        Document document;
        auto variant = data.at(i);
        if (variant.canConvert<Document>()) {
            document = variant.value<Document>();
        } else {
            document.setType(d->type);
            document.setAttributes(d->attributes);
            document.setAttributeValues(d->method != Method::Delete ? variant.toMap() : QVariantMap{});
        }

        QJsonObject dataObject;
        dataObject[u"data"] = document.toJsonObject();

        auto path = d->paths.size() > 0 ? d->paths.at(i) : d->path;
        auto relationships = document.relationshipValues().keys().join(u',');
        if (!relationships.isEmpty()) {
            relationships = u"include="_s + relationships;
        }

        ApiResultPtr result;
        switch(d->method) {
            case Post:
                result = Api::instance()->post(path, dataObject, relationships);
                break;
            case Patch:
                result = Api::instance()->patch(path, dataObject, relationships);
                break;
            case Delete:
                result = Api::instance()->del(path, dataObject, relationships);
                break;
        }

        connect(result.get(), &ApiResult::success, this, &Request::onSuccess, Qt::QueuedConnection);
        connect(result.get(), &ApiResult::error, this, &Request::onError, Qt::QueuedConnection);
        d->results.push_back(std::move(result));
    }
}

ApiResult* Request::result() const
{
    if (!d->results.empty()) {
        return d->results.front().get();
    }
    return nullptr;
}

void Request::onSuccess(const ApiResult* result)
{
    Api::instance()->cache()->remove(result->url());
    Q_EMIT success();
}

void Request::onError(const ApiResult* result)
{
    qCDebug(JSONAPI) << result->errorString();
    d->errors.append(result->errorString());
    Q_EMIT error();
}

}
}
