/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <QQuickAsyncImageProvider>

#include "jsonapi_export.h"

namespace Zax {
namespace JsonApi {

class ApiResult;

class JSONAPI_EXPORT ImageProvider : public QQuickAsyncImageProvider
{
public:
    /**
     * @todo write docs
     */
    ImageProvider();

    /**
     * @todo write docs
     */
    ~ImageProvider();

    QQuickImageResponse* requestImageResponse(const QString& id, const QSize& requestedSize) override;
};

class JSONAPI_EXPORT ImageResponse : public QQuickImageResponse
{
    Q_OBJECT
public:
    ImageResponse(const QString& id, const QSize& requestedSize);

    QQuickTextureFactory* textureFactory() const override;

    void jsonSuccess();
    void imageSuccess();

    static bool validateJson(ApiResult* result);

private:
    QString m_path;
    QString m_filters;
    QSize m_size;
    QImage m_image;

    std::unique_ptr<ApiResult> m_jsonResult;
    std::unique_ptr<ApiResult> m_imageResult;
};

} // namespace JsonApi
} // namespace Zax
