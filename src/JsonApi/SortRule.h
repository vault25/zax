/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <QVariant>
#include "QueryRule.h"

#include "jsonapi_export.h"

namespace Zax {
namespace JsonApi {

/**
 * @todo write docs
 */
class JSONAPI_EXPORT SortRule : public QueryRule
{
    Q_OBJECT
    QML_ELEMENT

public:
    enum Direction {
        Ascending,
        Descending
    };
    Q_ENUM(Direction)

    SortRule(QObject* parent = nullptr);

    void modifyQuery(QUrlQuery& query) override;
    void modifyResult(const QJsonDocument& document, QList<Document>& output) override;

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QString field READ field WRITE setField NOTIFY fieldChanged)
    QString field() const;
    void setField(const QString & newField);
    Q_SIGNAL void fieldChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(Direction direction READ direction WRITE setDirection NOTIFY directionChanged)
    Direction direction() const;
    void setDirection(Direction newDirection);
    Q_SIGNAL void directionChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QStringList fields READ fields WRITE setFields NOTIFY fieldsChanged)
    QStringList fields() const;
    void setFields(const QStringList& newFields);
    Q_SIGNAL void fieldsChanged();

private:
    QString m_field;
    Direction m_direction = Ascending;
    QStringList m_fields;
};

}
}
