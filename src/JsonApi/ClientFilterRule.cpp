/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "ClientFilterRule.h"

#include <QDebug>

#include "Document.h"

using namespace Zax::JsonApi;

template <typename T>
bool findRecursive(const T& container, const QVariant& findValue)
{
    bool found = false;

    for (auto value : container)
    {
        if (value == findValue) {
            found = true;
            break;
        }

        if (value.typeId() == QMetaType::QVariantList) {
            if (findRecursive(value.toList(), findValue)) {
                found = true;
                break;
            }
        }

        if (value.typeId() == QMetaType::QVariantMap) {
            if (findRecursive(value.toMap(), findValue)) {
                found = true;
                break;
            }
        }
    }

    return found;
}

ClientFilterRule::ClientFilterRule(QObject* parent)
    : FilterRule(parent)
{
}

void ClientFilterRule::modifyQuery(QUrlQuery& query)
{
    Q_UNUSED(query);
}

void ClientFilterRule::modifyResult(const QJsonDocument& document, QList<Document>& output)
{
    Q_UNUSED(document);

    auto fieldName = field();
    auto checkValue = value();
    auto emptyMode = m_empty;

    auto itr = std::remove_if(output.begin(), output.end(), [fieldName, checkValue, emptyMode](const Document& item) {
        if (auto object = item.attributeValues(); object.contains(fieldName)) {
            auto field = object.value(fieldName);

            if (field.typeId() == QMetaType::QVariantList) {
                const auto fieldList = field.toList();

                if (fieldList.isEmpty() && emptyMode == AcceptEmpty) {
                    return false;
                }

                if (findRecursive(fieldList, checkValue)) {
                    return false;
                }

                return true;
            }

            if (field.typeId() == QMetaType::QVariantMap) {
                const auto fieldMap = field.toMap();

                if (fieldMap.isEmpty() && emptyMode == AcceptEmpty) {
                    return false;
                }

                if (findRecursive(fieldMap, checkValue)) {
                    return false;
                }

                return true;
            }

            return field != checkValue;
        } else {
            return true;
        }
    });

    output.erase(itr, output.end());
}

ClientFilterRule::EmptyMode ClientFilterRule::empty() const
{
    return m_empty;
}

void ClientFilterRule::setEmpty(EmptyMode newEmpty)
{
    if (newEmpty == m_empty) {
        return;
    }

    m_empty = newEmpty;

    Q_EMIT emptyChanged();
}
