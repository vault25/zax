// SPDX-FileCopyrightText: 2023 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include <functional>

#include <QJsonObject>
#include <QObject>
#include <qqmlregistration.h>

#include "jsonapi_export.h"

namespace Zax {
namespace JsonApi {

class JSONAPI_EXPORT Document {
    Q_GADGET
    QML_VALUE_TYPE(document)
    QML_STRUCTURED_VALUE

public:
    Q_INVOKABLE Document();

    bool isEmpty();

    Q_PROPERTY(QString id READ id WRITE setId)
    QString id() const;
    void setId(const QString& newId);

    Q_PROPERTY(QString type READ type WRITE setType)
    QString type() const;
    void setType(const QString& newType);

    Q_PROPERTY(QVariantMap attributes READ attributes WRITE setAttributes)
    QVariantMap attributes() const;
    void setAttributes(const QVariantMap& newAttributes);

    Q_PROPERTY(QVariantMap values READ attributeValues WRITE setAttributeValues)
    QVariantMap attributeValues() const;
    void setAttributeValues(const QVariantMap& data);
    void setAttributeValues(const QJsonObject& data);

    QVariant attributeValue(QStringView attribute);
    Q_INVOKABLE void setAttributeValue(const QString &attribute, const QVariant& value);
    void setAttributeValue(const QString& attribute, const QJsonValue& value);

    Q_PROPERTY(QVariantMap relationships READ relationships WRITE setRelationships)
    QVariantMap relationships() const;
    void setRelationships(const QVariantMap &newRelationships);

    QVariantMap relationshipValues();
    Q_INVOKABLE void setRelationshipValue(const QString &relationship, const QVariant& value);

    QJsonObject toJsonObject();

    using FromJsonFunction = std::function<QVariant(const QJsonValue&)>;
    using ToJsonFunction = std::function<QJsonValue(const QVariant&)>;
    static void registerAttributeType(QStringView fieldType, const FromJsonFunction& convertFromJson, const ToJsonFunction& convertToJson);

    static Document fromJsonObject(const QJsonObject& object, const QVariantMap& attributes, const QVariantMap& relationships = QVariantMap{});
    static Document fromJsonObject(const QJsonObject& object, const Document& docTemplate);

private:
    QVariant valueFromJson(const QString& attribute, const QJsonValue& value) const;
    QJsonValue valueToJson(const QString& attribute) const;

    QString m_id;
    QString m_type;
    QVariantMap m_attributes;
    QVariantMap m_attributeValues;
    QVariantMap m_relationships;
    QVariantMap m_relationshipValues;

    static QHash<QString, std::pair<FromJsonFunction, ToJsonFunction>> s_attributeTypes;
};

} // namespace JsonApi
} // namespace Zax

Q_DECLARE_METATYPE(Zax::JsonApi::Document)

JSONAPI_EXPORT QDebug operator<<(QDebug debug, const Zax::JsonApi::Document &document);
