/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <QVariant>
#include "FilterRule.h"

#include "jsonapi_export.h"

namespace Zax {
namespace JsonApi {

/**
 * @todo write docs
 */
class JSONAPI_EXPORT ClientFilterRule : public FilterRule
{
    Q_OBJECT
    QML_ELEMENT

public:
    enum EmptyMode {
        AcceptEmpty,
        RejectEmpty
    };
    Q_ENUM(EmptyMode)

    ClientFilterRule(QObject* parent = nullptr);

    void modifyQuery(QUrlQuery& query) override;
    void modifyResult(const QJsonDocument& document, QList<Document>& output) override;

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(EmptyMode empty READ empty WRITE setEmpty NOTIFY emptyChanged)
    EmptyMode empty() const;
    void setEmpty(EmptyMode newEmpty);
    Q_SIGNAL void emptyChanged();

private:
    EmptyMode m_empty = RejectEmpty;
};

}
}
