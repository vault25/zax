/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "Query.h"

#include <QDebug>
#include <QVariantMap>
#include <QTimer>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QQmlEngine>

#include "Api.h"
#include "ApiResult.h"
#include "Document.h"
#include "QueryRule.h"

#include "jsonapi_logging.h"

using namespace Zax::JsonApi;

class JSONAPI_NO_EXPORT Query::Private
{
public:
    Private(Query* qq) : q(qq) { }

    void setRunning(bool newRunning);
    QString buildQuery();
    void get(Cache::CacheMode cache);

    Query* q;

    QString path;
    QVector<QueryRule*> rules;

    Cache::CacheMode cacheMode = Cache::CacheMode::Cache;

    bool refreshOnChange = false;

    int retryCount = 3;
    int tries = 0;
    std::chrono::milliseconds retryInterval = std::chrono::milliseconds(1000);
    std::chrono::milliseconds refreshInterval = std::chrono::milliseconds::zero();

    bool enabled = true;
    bool running = false;
    bool completed = true;
    bool shouldRunOnCompleted = false;

    std::unique_ptr<ApiResult> result;

    QVariantMap attributes;
    QVariantMap relationships;

    QList<Document> entries;
};

Query::Query(QObject* parent)
    : QObject(parent)
    , d(new Private(this))
{
}

Query::~Query() = default;

bool Query::enabled() const
{
    return d->enabled;
}

void Query::setEnabled(bool newEnabled)
{
    if (newEnabled == d->enabled) {
        return;
    }

    d->enabled = newEnabled;
    if (!d->enabled) {
        d->setRunning(false);
        d->shouldRunOnCompleted = false;
    }
    Q_EMIT enabledChanged();
}

QString Query::path() const
{
    return d->path;
}

void Query::setPath(const QString & newPath)
{
    if (newPath == d->path) {
        return;
    }

    d->path = newPath;
    refresh();
    Q_EMIT pathChanged();
}

QQmlListProperty<QueryRule> Query::rulesProperty()
{
    return QQmlListProperty<QueryRule>(this,
                                       nullptr,
                                       &Query::appendRule,
                                       &Query::ruleCount,
                                       &Query::ruleAt,
                                       &Query::clearRules,
                                       &Query::replaceRule,
                                       &Query::removeLastRule);
}

QVector<QueryRule*> Query::rules() const
{
    return d->rules;
}

void Query::setRules(const QVector<QueryRule*>& newRules)
{
    if (newRules == d->rules) {
        return;
    }

    for (auto rule : std::as_const(d->rules)) {
        disconnect(rule, &QueryRule::changed, this, &Query::onChanged);
    }

    d->rules = newRules;

    for (auto rule : std::as_const(d->rules)) {
        connect(rule, &QueryRule::changed, this, &Query::onChanged);
    }

    Q_EMIT rulesChanged();
}

bool Query::cacheResults() const
{
    return d->cacheMode == Cache::CacheMode::Cache;
}

void Query::setCacheResults(bool newCacheResults)
{
    if (newCacheResults == cacheResults()) {
        return;
    }

    d->cacheMode = newCacheResults ? Cache::CacheMode::Cache : Cache::CacheMode::DoNotCache;
    Q_EMIT cacheResultsChanged();
}

int Query::retryCount() const
{
    return d->retryCount;
}

void Query::setRetryCount(int newRetryCount)
{
    if (newRetryCount == d->retryCount) {
        return;
    }

    d->retryCount = newRetryCount;
    Q_EMIT retryCountChanged();
}

int Query::retryInterval() const
{
    return d->retryInterval.count();
}

void Query::setRetryInterval(int newRetryInterval)
{
    setRetryInterval(std::chrono::milliseconds(newRetryInterval));
}

void Query::setRetryInterval(std::chrono::milliseconds newRetryInterval)
{
    if (newRetryInterval == d->retryInterval) {
        return;
    }

    d->retryInterval = newRetryInterval;
    Q_EMIT retryIntervalChanged();
}

int Query::refreshInterval() const
{
    return d->refreshInterval.count();
}

void Query::setRefreshInterval(int newRefreshInterval)
{
    setRefreshInterval(std::chrono::milliseconds(newRefreshInterval));
}

void Query::setRefreshInterval(std::chrono::milliseconds newRefeshInterval)
{
    if (newRefeshInterval == d->refreshInterval) {
        return;
    }

    d->refreshInterval = newRefeshInterval;
    Q_EMIT refreshIntervalChanged();
}

bool Query::refreshOnChange() const
{
    return d->refreshOnChange;
}

void Query::setRefreshOnChange(bool newRefreshOnChange)
{
    if (newRefreshOnChange == d->refreshOnChange) {
        return;
    }

    d->refreshOnChange = newRefreshOnChange;
    Q_EMIT refreshOnChangeChanged();
}

QVariantMap Query::attributes() const
{
    return d->attributes;
}

void Query::setAttributes(const QVariantMap& newAttributes)
{
    if (newAttributes == d->attributes) {
        return;
    }

    d->attributes = newAttributes;
    Q_EMIT attributesChanged();
}

QVariantMap Zax::JsonApi::Query::relationships() const
{
    return d->relationships;
}

void Zax::JsonApi::Query::setRelationships(const QVariantMap& newRelationships)
{
    if (newRelationships == d->relationships) {
        return;
    }

    d->relationships = newRelationships;
    Q_EMIT relationshipsChanged();
}

bool Query::running() const
{
    return d->running;
}

void Query::setRunning(bool newRunning)
{
    if (newRunning == d->running) {
        return;
    }

    if (!d->completed) {
        d->shouldRunOnCompleted = newRunning;
        return;
    }

    if (newRunning) {
        run();
    } else {
        d->setRunning(false);
    }
}

void Query::run()
{
    if (!d->enabled || d->running) {
        return;
    }

    if (!d->completed) {
        d->shouldRunOnCompleted = true;
        return;
    }

    d->tries = d->retryCount;
    d->setRunning(true);
    d->get(d->cacheMode);
}

void Query::refresh()
{
    if (!d->enabled || d->running || !d->completed) {
        return;
    }

    d->tries = d->retryCount;
    d->setRunning(true);
    d->get(Cache::CacheMode::ForceRefresh);
}

void Query::removeFromCache()
{
    auto url = Api::instance()->buildUrl(d->path, d->buildQuery());
    Api::instance()->cache()->remove(url);
}

ApiResult* Query::result() const
{
    return d->result.get();
}

QList<Document> Query::entries() const
{
    return d->entries;
}

void Query::classBegin()
{
    d->completed = false;
}

void Query::componentComplete()
{
    d->completed = true;
    if (d->shouldRunOnCompleted) {
        run();
    }
}

void Query::onSuccess()
{
    auto document = d->result->asJson().value_or(QJsonDocument{});
    const auto data = document.object().value(u"data");

    auto entries = QJsonArray();
    if (data.isArray()) {
        entries = data.toArray();
    } else if (data.isObject()) {
        entries.append(data.toObject());
    } else {
        return;
    }

    d->entries.clear();
    for (auto item : entries) {
        auto document = Document::fromJsonObject(item.toObject(), d->attributes, d->relationships);
        if (!document.isEmpty()) {
            d->entries.append(document);
        }
    }

    for (auto rule : d->rules) {
        rule->modifyResult(document, d->entries);
    }

    Q_EMIT entriesChanged();

    if (d->refreshInterval > std::chrono::milliseconds::zero()) {
        QTimer::singleShot(d->refreshInterval, this, [this]() {
            d->get(d->cacheMode);
        });
    }

    d->setRunning(false);
    Q_EMIT success();
}

void Query::onError()
{
    if (d->tries > 0) {
        d->tries--;
        QTimer::singleShot(d->retryInterval, this, [this]() {
            d->get(d->cacheMode);
        });
    } else {
        auto errorCode = d->result->errorCode();

        qCDebug(JSONAPI) << "Error retrieving data for" << this << ":" << errorCode << d->result->errorString();

        if (d->cacheMode != Cache::CacheMode::DoNotCache
            && errorCode != QNetworkReply::ServiceUnavailableError
            && errorCode != QNetworkReply::UnknownContentError
            && errorCode != QNetworkReply::InternalServerError) {
            qCDebug(JSONAPI) << "Attempting to retrieve from cache instead";
            d->get(Cache::CacheMode::CacheOnly);
            return;
        }

        d->setRunning(false);
        Q_EMIT error();
    }
}

void Query::onChanged()
{
    if (d->refreshOnChange) {
        run();
    }

    Q_EMIT changed();
}

void Query::appendRule(QQmlListProperty<QueryRule>* property, QueryRule* rule)
{
    auto query = qobject_cast<Query*>(property->object);
    connect(rule, &QueryRule::changed, query, &Query::onChanged);
    query->d->rules.append(rule);
    Q_EMIT query->rulesChanged();
}

qsizetype Query::ruleCount(QQmlListProperty<QueryRule>* property)
{
    auto query = qobject_cast<Query*>(property->object);
    return query->d->rules.size();
}

QueryRule* Query::ruleAt(QQmlListProperty<QueryRule>* property, qsizetype index)
{
    auto query = qobject_cast<Query*>(property->object);
    return query->d->rules.at(index);
}

void Query::clearRules(QQmlListProperty<QueryRule>* property)
{
    auto query = qobject_cast<Query*>(property->object);
    query->d->rules.clear();
    Q_EMIT query->rulesChanged();
}

void Query::replaceRule(QQmlListProperty<QueryRule>* property, qsizetype index, QueryRule* rule)
{
    auto query = qobject_cast<Query*>(property->object);
    auto oldRule = query->d->rules.at(index);
    disconnect(oldRule, &QueryRule::changed, query, &Query::onChanged);
    query->d->rules.replace(index, rule);
    connect(rule, &QueryRule::changed, query, &Query::onChanged);
    Q_EMIT query->rulesChanged();
}

void Query::removeLastRule(QQmlListProperty<QueryRule>* property)
{
    auto query = qobject_cast<Query*>(property->object);
    auto oldRule = query->d->rules.takeLast();
    disconnect(oldRule, &QueryRule::changed, query, &Query::onChanged);
    Q_EMIT query->rulesChanged();
}

void Query::Private::setRunning(bool newRunning)
{
    if (running == newRunning) {
        return;
    }

    running = newRunning;
    Q_EMIT q->runningChanged();
}

QString Query::Private::buildQuery()
{
    QUrlQuery query;
    for (const auto& rule : std::as_const(rules)) {
        rule->modifyQuery(query);
    }
    return query.toString();
}

void Query::Private::get(Cache::CacheMode cache)
{
    setRunning(true);

    result = Api::instance()->get(path, buildQuery(), cache);

    result->setValidationCallback(Api::validateJsonApi);

    connect(result.get(), &ApiResult::success, q, &Query::onSuccess, Qt::QueuedConnection);
    connect(result.get(), &ApiResult::error, q, &Query::onError, Qt::QueuedConnection);
}

QDebug operator<<(QDebug debug, const Query* input)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << "Zax::JsonApi::Query(path=" << input->path() << ", query=" /*<< input->d->buildQuery()*/ << ")";
    return debug;
}

QDebug operator<<(QDebug debug, const std::unique_ptr<Query>& input)
{
    return operator<<(debug, input.get());
}

QDebug operator<<(QDebug debug, const std::shared_ptr<Query>& input)
{
    return operator<<(debug, input.get());
}
