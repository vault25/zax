/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "SortRule.h"

#include <QUrlQuery>
#include <QStringBuilder>

using namespace Zax::JsonApi;

SortRule::SortRule(QObject* parent)
    : QueryRule(parent)
{
}

void SortRule::modifyQuery(QUrlQuery& query)
{
    QStringList fields = m_fields;
    if (fields.isEmpty() && !m_field.isEmpty()) {
        fields.append(m_field);
    }

    if (fields.isEmpty()) {
        return;
    }

    auto dir = m_direction == Ascending ? QStringLiteral("") : QStringLiteral("-");
    fields[0] = dir % fields[0];

    query.addQueryItem(QStringLiteral("sort"), fields.join(QLatin1Char(',')));
}

void SortRule::modifyResult(const QJsonDocument& document, QList<Document>& output)
{
    Q_UNUSED(document);
    Q_UNUSED(output);
}

QString SortRule::field() const
{
    return m_field;
}

void SortRule::setField(const QString& newField)
{
    if (newField == m_field) {
        return;
    }

    m_field = newField;
    Q_EMIT fieldChanged();
    Q_EMIT changed();
}

SortRule::Direction SortRule::direction() const
{
    return m_direction;
}

void SortRule::setDirection(Direction newDirection)
{
    if (newDirection == m_direction) {
        return;
    }

    m_direction = newDirection;
    Q_EMIT directionChanged();
    Q_EMIT changed();
}

QStringList SortRule::fields() const
{
    return m_fields;
}

void SortRule::setFields(const QStringList& newFields)
{
    if (newFields == m_fields) {
        return;
    }

    m_fields = newFields;
    Q_EMIT fieldsChanged();
    Q_EMIT changed();
}
