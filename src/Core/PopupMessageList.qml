import QtQuick
import QtQuick.Controls

ListView {
    id: control

    property int duration: 10000

    function show(message: string): void {
        model.append({"message": message})
    }

    model: ListModel { }

    interactive: false
    verticalLayoutDirection: ListView.BottomToTop

    delegate: ItemDelegate {
        required property string message

        width: ListView.view.width

        text: message
    }

    Timer {
        running: control.count > 0
        interval: control.duration
        repeat: true
        onTriggered: {
            control.model.remove(0, 1)
        }
    }

    add: Transition {
        NumberAnimation { properties: "opacity"; from: 0; to: 1; duration: 250 }
    }

    remove: Transition {
        NumberAnimation { properties: "opacity"; from: 1; to: 0; duration: 250 }
    }

    displaced: Transition {
        NumberAnimation { properties: "y"; duration: 250 }
    }

    populate: Transition {
        NumberAnimation { properties: "opacity"; from: 0; to: 1; duration: 250 }
    }
}
