// SPDX-FileCopyrightText: 2023 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include <QObject>
#include <QQmlEngine>
#include <QQmlPropertyMap>
#include <QCommandLineParser>
#include <qqmlregistration.h>

#include "core_export.h"

namespace Zax {
namespace Core {

struct CORE_EXPORT CommandLineOption
{
    CommandLineOption(const QString& name, const QString& description, QMetaType::Type type = QMetaType::Bool, const QString& valueName = QString{});

    QString name;
    QString description;
    QString valueName;
    QMetaType::Type type;
};

class CORE_EXPORT CommandLineParser : public QQmlPropertyMap
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

public:
    ~CommandLineParser() override;

    void addOption(const CommandLineOption& option);
    void addOptions(const QList<CommandLineOption>& options);
    void process();

    bool isSet(QAnyStringView name);

    template <typename T>
    T value(QAnyStringView name)
    {
        return QQmlPropertyMap::value(name.toString()).value<T>();
    }

    static std::shared_ptr<CommandLineParser> instance();
    static CommandLineParser* create(QQmlEngine*, QJSEngine*);

private:
    CommandLineParser();

    class Private;
    const std::unique_ptr<Private> d;
};

}
}
