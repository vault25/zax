import QtQuick
import QtQuick.Controls
import QtQuick.Window

import "." as Core

ApplicationWindow {
    id: window

    visible: true
    width: Math.min(800, Screen.width)
    height: Math.min(600, Screen.height)

    visibility: Qt.platform.os == "android" ? Window.FullScreen : Window.AutomaticVisibility

    property int state: Core.ApplicationState.State.Initial;

    property list<ApplicationState> states

    property alias stack: stackView

    property int currentIndex: -1

    property alias currentItem: stackView.currentItem

    property alias leftPadding: stackView.anchors.leftMargin
    property alias rightPadding: stackView.anchors.rightMargin
    property alias topPadding: stackView.anchors.topMargin
    property alias bottomPadding: stackView.anchors.bottomMargin

    property alias messages: popupMessageList

    onCurrentIndexChanged: {
        if (currentIndex < 0 || currentIndex >= states.length) {
            return
        }

        let lastActive = states[d.lastActiveIndex]
        if (lastActive) {
            lastActive.deactivated()
        }

        let s = states[currentIndex]
        while (s.skip) {
            currentIndex++
            s = states[currentIndex]
        }

        window.state = s.state

        stackView.replace(s.page, s.initialProperties)

        s.activated()

        d.lastActiveIndex = currentIndex
    }

    onStateChanged: {
        if (states[currentIndex].state == window.state) {
            return
        }

        for (let i in states) {
            if (states[i].state == window.state && !states[i].skip) {
                currentIndex = i
                return
            }
        }
    }

    StackView {
        id: stackView
        anchors.fill: parent
    }

    PopupMessageList {
        id: popupMessageList

        parent: Overlay.overlay
        z: 9999

        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
        }

        height: parent.height * 0.25
        width: parent.width * 0.5
    }

    Timer {
        id: stateChangeTimer

        interval: window.states[window.currentIndex].duration
        running: interval > 0

        onTriggered: {
            window.currentIndex += 1
        }
    }

    Component.onCompleted: window.currentIndex = 0

    QtObject {
        id: d

        property int lastActiveIndex: -1
    }
}

