// SPDX-FileCopyrightText: 2023 Arjen Hiemstra <ahiemstra@heimr.nl>
// 
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "CommandLineParser.h"

using namespace Zax::Core;

CommandLineOption::CommandLineOption(const QString& _name, const QString& _description, QMetaType::Type _type, const QString& _valueName)
    : name(_name)
    , description(_description)
    , valueName(_valueName)
    , type(_type)
{
}

class CORE_NO_EXPORT CommandLineParser::Private
{
public:
    QCommandLineParser parser;
    // QStringList optionNames;
    QList<CommandLineOption> options;
};

CommandLineParser::CommandLineParser()
    : QQmlPropertyMap(nullptr)
    , d(std::make_unique<Private>())
{
}

CommandLineParser::~CommandLineParser() = default;

void CommandLineParser::addOption(const CommandLineOption& option)
{
    d->options.append(option);
    QCommandLineOption qtOption(option.name);
    qtOption.setDescription(option.description);
    if (option.type != QMetaType::Bool) {
        qtOption.setValueName(option.valueName.isEmpty() ? option.name : option.valueName);
    }
    d->parser.addOption(qtOption);
}

void CommandLineParser::addOptions(const QList<CommandLineOption>& options)
{
    for (auto option : options) {
        addOption(option);
    }
}

void CommandLineParser::process()
{
    d->parser.addVersionOption();
    d->parser.addHelpOption();
    d->parser.process(*QCoreApplication::instance());

    for (auto option : std::as_const(d->options)) {
        QVariant value;
        if (option.type != QMetaType::Bool) {
            value = QVariant::fromValue(d->parser.value(option.valueName.isEmpty() ? option.name : option.valueName));
            value.convert(QMetaType(option.type));
        } else {
            value = d->parser.isSet(option.name) ? true : false;
        }
        insert(option.name, value);
    }

    freeze();
}

bool Zax::Core::CommandLineParser::isSet(QAnyStringView name)
{
    return d->parser.isSet(name.toString());
}

std::shared_ptr<CommandLineParser> CommandLineParser::instance()
{
    static auto instance = std::shared_ptr<CommandLineParser>(new CommandLineParser);
    return instance;
}

CommandLineParser* CommandLineParser::create(QQmlEngine*, QJSEngine*)
{
    auto self = instance().get();
    QQmlEngine::setObjectOwnership(self, QQmlEngine::CppOwnership);
    return self;
}
