import QtQml

QtObject {
    enum State {
        Initial,
        Opening,
        Starting,
        Operational,
        Error
    }

    property int state
    property var page
    property var initialProperties
    property int duration: -1
    property bool skip: false

    signal activated()
    signal deactivated()
}
