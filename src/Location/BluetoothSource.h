/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "LocationSource.h"

#include "location_export.h"

namespace Zax {
namespace Location {

class LOCATION_EXPORT BluetoothSource : public LocationSource
{
    Q_OBJECT

public:
    BluetoothSource(QObject* parent = nullptr);

    qreal distanceTo(LocationSource* other) override;

protected:
    void setCoordinate(const QGeoCoordinate& newCoordinate) override;

    bool supportsCoordinate() const override;

};

}
}
