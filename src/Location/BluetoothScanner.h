/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <memory>

#include <QObject>
// #include <QVector>
#include <QBluetoothDeviceInfo>

#include "BluetoothDevice.h"

#include "location_export.h"

namespace Zax {
namespace Location {

class BluetoothDevice;

class LOCATION_EXPORT BluetoothScanner : public QObject
{
    Q_OBJECT

public:
    BluetoothScanner(QObject* parent = nullptr);
    ~BluetoothScanner() override;

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)
    bool enabled() const;
    void setEnabled(bool newEnabled);
    Q_SIGNAL void enabledChanged();

    Q_PROPERTY(QList<Zax::Location::BluetoothDevice*> devices READ devices NOTIFY devicesChanged)
    QList<BluetoothDevice*> devices() const;
    BluetoothDevice* device(const QString& deviceId) const;
    Q_SIGNAL void devicesChanged();
    Q_SIGNAL void deviceFound(BluetoothDevice* device);
    Q_SIGNAL void deviceLost(BluetoothDevice* device);

    static std::shared_ptr<BluetoothScanner> instance();

// public Q_SLOTS:
//     void startDiscovery();
//     void stopDiscovery();

private:
//     void onDeviceDiscovered(const QBluetoothDeviceInfo& deviceInfo);
//     void onDeviceUpdated(const QBluetoothDeviceInfo& deviceInfo, QBluetoothDeviceInfo::Fields fields);
    void onDiscoveryFinished();

    class Private;
    const std::unique_ptr<Private> d;
};

} // namespace Location
} // namespace Zax
