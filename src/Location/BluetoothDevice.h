/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <chrono>

#include <QObject>
#include <QDateTime>

#include "location_export.h"

class QBluetoothDeviceInfo;

namespace Zax {
namespace Location {

/**
 * @todo write docs
 */
class LOCATION_EXPORT BluetoothDevice : public QObject
{
    Q_OBJECT

public:
    BluetoothDevice(const QString& address, QObject* parent = nullptr);
    ~BluetoothDevice();

    Q_PROPERTY(QString address READ address CONSTANT)
    QString address() const;

    Q_PROPERTY(QString name READ name NOTIFY nameChanged)
    QString name() const;
    void setName(const QString& name);
    Q_SIGNAL void nameChanged();

    Q_PROPERTY(int rssi READ rssi NOTIFY rssiChanged)
    int rssi() const;
    void setRssi(int rssi);
    Q_SIGNAL void rssiChanged();

    Q_PROPERTY(double distance READ distance NOTIFY rssiChanged)
    double distance() const;

    std::chrono::steady_clock::time_point lastSeen() const;
    void markAsSeen();

//     void setLastSeen(const std::chrono::steady_clock::time_point& newLastSeen);

    static BluetoothDevice* fromDeviceInfo(const QBluetoothDeviceInfo& info);

private:
    QString m_address;
    QString m_name;
    int m_rssi = 0;
    double m_distance = 0.0f;
    std::chrono::steady_clock::time_point m_lastSeen;
};

} // namespace Location
} // namespace Zax
