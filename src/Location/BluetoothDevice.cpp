/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "BluetoothDevice.h"

#include <cmath>

#include <QBluetoothDeviceInfo>
#include <QBluetoothAddress>

#include <QDebug>

using namespace Zax::Location;

const double rssiBaseline = -60.0;
const double rssiEnvironment = 3.0;
const double rssiC1 = 0.89976;
const double rssiC2 = 7.7095;
const double rssiC3 = 0.111;

double distanceFromRssi(int rssi)
{
    auto result = 0.0;
    auto ratio = double(rssi) * 1.0 / rssiBaseline;
    if(ratio < 1.0)
    {
        result = pow(ratio, 10.0);
    }
    else
    {
        result = rssiC1 * pow(ratio, rssiC2) + rssiC3;
    }
    return result;
}

BluetoothDevice::BluetoothDevice(const QString& address, QObject* parent)
    : QObject(parent), m_address(address)
{
    m_lastSeen = std::chrono::steady_clock::now();
}


BluetoothDevice::~BluetoothDevice()
{
}

QString BluetoothDevice::address() const
{
    return m_address;
}

QString BluetoothDevice::name() const
{
    return m_name;
}

void BluetoothDevice::setName(const QString& name)
{
    if(name == m_name)
        return;

    m_name = name;
    Q_EMIT nameChanged();
}

int BluetoothDevice::rssi() const
{
    return m_rssi;
}

void BluetoothDevice::setRssi(int rssi)
{
    if(rssi == m_rssi)
        return;

    m_rssi = rssi;
    m_distance = distanceFromRssi(m_rssi);

    Q_EMIT rssiChanged();
}

double BluetoothDevice::distance() const
{
    return m_distance;
}

std::chrono::steady_clock::time_point BluetoothDevice::lastSeen() const
{
    return m_lastSeen;
}

void BluetoothDevice::markAsSeen()
{
    m_lastSeen = std::chrono::steady_clock::now();
}

BluetoothDevice* BluetoothDevice::fromDeviceInfo(const QBluetoothDeviceInfo& info)
{
    auto newDevice = new BluetoothDevice{info.address().toString()};
    newDevice->setName(info.name());
    newDevice->setRssi(info.rssi());
    return newDevice;
}
