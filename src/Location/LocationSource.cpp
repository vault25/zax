/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "LocationSource.h"

using namespace Zax::Location;

class LOCATION_NO_EXPORT LocationSource::Private
{
public:
    QString identifier;
    QGeoCoordinate coordinate;
    qreal triggerDistance = -1.0;
};

LocationSource::LocationSource(QObject* parent)
    : QObject(parent)
    , d(std::make_unique<Private>())
{
}

LocationSource::~LocationSource() = default;

QString LocationSource::identifier() const
{
    return d->identifier;
}

void LocationSource::setIdentifier(const QString & newId)
{
    if (newId == d->identifier) {
        return;
    }

    d->identifier = newId;
    Q_EMIT identifierChanged();
}

QGeoCoordinate LocationSource::coordinate() const
{
    return d->coordinate;
}

void LocationSource::setCoordinate(const QGeoCoordinate& newCoordinate)
{
    if (newCoordinate == d->coordinate) {
        return;
    }

    d->coordinate = newCoordinate;
    Q_EMIT coordinateChanged();
}

qreal LocationSource::triggerDistance() const
{
    return d->triggerDistance;
}

void LocationSource::setTriggerDistance(qreal newTriggerDistance)
{
    if (newTriggerDistance == d->triggerDistance) {
        return;
    }

    d->triggerDistance = newTriggerDistance;
    Q_EMIT triggerDistanceChanged();
}

bool LocationSource::supportsCoordinate() const
{
    return true;
}


qreal LocationSource::distanceTo(LocationSource* other)
{
    if (other == this) {
        return 0.0;
    }

    if (!other->supportsCoordinate()) {
        return other->distanceTo(this);
    }

    if (d->coordinate.isValid() && other->coordinate().isValid()) {
        return d->coordinate.distanceTo(other->coordinate());
    }

    return -1.0;
}

qreal LocationSource::azimuthTo(LocationSource* other)
{
    if (other == this) {
        return 0.0;
    }

    if (!other->supportsCoordinate()) {
        return other->azimuthTo(this);
    }

    if (d->coordinate.isValid() && other->coordinate().isValid()) {
        return other->coordinate().azimuthTo(d->coordinate);
    }

    return -1.0;
}
