/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "QmlPlugin.h"

#include <QQmlEngine>

#include "ProximitySensor.h"
#include "LocationSource.h"
#include "DeviceLocationSource.h"

#include "BluetoothScanner.h"
#include "BluetoothDevice.h"
#include "BluetoothSource.h"

using namespace Zax::Location;

void QmlPlugin::registerTypes(const char* uri)
{
    Q_ASSERT(QLatin1String(uri) == QLatin1String("Zax.Location"));

    qmlRegisterType<ProximitySensor>(uri, 1, 0, "ProximitySensor");
    qmlRegisterType<LocationSource>(uri, 1, 0, "LocationSource");
    qmlRegisterType<DeviceLocationSource>(uri, 1, 0, "DeviceLocationSource");
    qmlRegisterType<BluetoothSource>(uri, 1, 0, "BluetoothSource");

    qmlRegisterSingletonType<BluetoothScanner>(uri, 1, 0, "BluetoothScanner", [](QQmlEngine*, QJSEngine*) {
        auto scanner = BluetoothScanner::instance().get();
        QQmlEngine::setObjectOwnership(scanner, QQmlEngine::CppOwnership);
        return scanner;
    });
    qmlRegisterUncreatableType<BluetoothDevice>(uri, 1, 0, "BluetoothDevice", QStringLiteral("Created by BluetoothScanner"));
}
