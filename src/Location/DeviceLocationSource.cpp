/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "DeviceLocationSource.h"

#include <QGeoPositionInfoSource>

using namespace Zax::Location;

class LOCATION_NO_EXPORT DeviceLocationSource::Private
{
public:
    QGeoPositionInfoSource* positionInfoSource;
    qreal minimumAccuracy = -1.0;
    qreal accuracy = -1.0;
};

DeviceLocationSource::DeviceLocationSource(QObject* parent)
    : LocationSource(parent)
    , d(std::make_unique<Private>())
{
    d->positionInfoSource = QGeoPositionInfoSource::createDefaultSource(this);
    connect(d->positionInfoSource, &QGeoPositionInfoSource::positionUpdated, this, &DeviceLocationSource::onPositionUpdated);
}

DeviceLocationSource::~DeviceLocationSource() = default;

int DeviceLocationSource::updateInterval() const
{
    return d->positionInfoSource->updateInterval();
}

void DeviceLocationSource::setUpdateInterval(std::chrono::milliseconds newUpdateInterval)
{
    setUpdateInterval(newUpdateInterval.count());
}

void DeviceLocationSource::setUpdateInterval(int newUpdateInterval)
{
    if (newUpdateInterval == d->positionInfoSource->updateInterval()) {
        return;
    }

    d->positionInfoSource->setUpdateInterval(newUpdateInterval);

    if (newUpdateInterval > 0) {
        d->positionInfoSource->startUpdates();
    } else {
        d->positionInfoSource->stopUpdates();
    }

    Q_EMIT updateIntervalChanged();
}

qreal DeviceLocationSource::minimumAccuracy() const
{
    return d->minimumAccuracy;
}

void DeviceLocationSource::setMinimumAccuracy(qreal newMinimumAccuracy)
{
    if (newMinimumAccuracy == d->minimumAccuracy) {
        return;
    }

    d->minimumAccuracy = newMinimumAccuracy;
    Q_EMIT minimumAccuracyChanged();
}

qreal DeviceLocationSource::accuracy() const
{
    return d->accuracy;
}

void DeviceLocationSource::onPositionUpdated(const QGeoPositionInfo& position)
{
    if (position.hasAttribute(QGeoPositionInfo::HorizontalAccuracy)) {
        auto accuracy = position.attribute(QGeoPositionInfo::HorizontalAccuracy);
        d->accuracy = accuracy;

        if (d->minimumAccuracy < 0.0 || accuracy < d->minimumAccuracy) {
            setCoordinate(position.coordinate());
        } else {
            setCoordinate(QGeoCoordinate{});
        }
    }
}
