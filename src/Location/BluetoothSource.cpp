/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "BluetoothSource.h"

#include "BluetoothScanner.h"
#include "BluetoothDevice.h"

#include "location_logging.h"

using namespace Zax::Location;

BluetoothSource::BluetoothSource(QObject* parent)
    : LocationSource(parent)
{
}

qreal BluetoothSource::distanceTo(LocationSource* other)
{
    if (other == this) {
        return 0.0;
    }

    auto device = BluetoothScanner::instance()->device(identifier());
    if (device) {
        return device->distance();
    }

    return -1.0;
}

void BluetoothSource::setCoordinate(const QGeoCoordinate& newCoordinate)
{
    Q_UNUSED(newCoordinate);

    qCWarning(LOCATION) << "BluetoothSource does not support coordinates";
}

bool BluetoothSource::supportsCoordinate() const
{
    return false;
}
