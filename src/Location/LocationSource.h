/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <memory>

#include <QObject>
#include <QGeoCoordinate>

#include "location_export.h"

namespace Zax {
namespace Location {

class LOCATION_EXPORT LocationSource : public QObject
{
    Q_OBJECT

public:
    LocationSource(QObject* parent = nullptr);
    ~LocationSource() override;

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QString identifier READ identifier WRITE setIdentifier NOTIFY identifierChanged)
    QString identifier() const;
    virtual void setIdentifier(const QString& newId);
    Q_SIGNAL void identifierChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QGeoCoordinate coordinate READ coordinate WRITE setCoordinate NOTIFY coordinateChanged)
    QGeoCoordinate coordinate() const;
    virtual void setCoordinate(const QGeoCoordinate& newCoordinate);
    Q_SIGNAL void coordinateChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(qreal triggerDistance READ triggerDistance WRITE setTriggerDistance NOTIFY triggerDistanceChanged)
    qreal triggerDistance() const;
    virtual void setTriggerDistance(qreal newTriggerDistance);
    Q_SIGNAL void triggerDistanceChanged();

    virtual bool supportsCoordinate() const;

    /**
     * Calculate the distance to another LocationSource.
     *
     * This will return the distance in meters to the other LocationSource, or
     * -1.0 if no distance can be calculated. This may happen if no GPS location
     * was yet acquired, for example.
     *
     * \note This should really use std::optional but that is not currently
     * usable from QML.
     */
    Q_INVOKABLE virtual qreal distanceTo(LocationSource* other);

    Q_INVOKABLE virtual qreal azimuthTo(LocationSource* other);

private:
    class Private;
    const std::unique_ptr<Private> d;
};

} // namespace Location
} // namespace Zax
