
add_library(Location)
add_library(Zax::Location ALIAS Location)

ecm_qt_declare_logging_category(location_EXTRA_SRCS
    HEADER location_logging.h
    IDENTIFIER LOCATION
    CATEGORY_NAME zax.location
    DEFAULT_SEVERITY ${DEFAULT_LOGGING_SEVERITY}
    DESCRIPTION "Zax - Location"
    EXPORT Zax
)

target_sources(Location PRIVATE
    ProximitySensor.cpp
    LocationSource.cpp
    DeviceLocationSource.cpp
    BluetoothScanner.cpp
    BluetoothDevice.cpp
    BluetoothSource.cpp
    ${location_EXTRA_SRCS}
)

target_link_libraries(Location PUBLIC Qt6::Gui Qt6::Qml Qt6::Positioning Qt6::Bluetooth)

target_include_directories(Location PUBLIC
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>"
    "$<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/src>"
    "$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/Zax/Location>"
    "$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/Zax>"
    "$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>"
)

set(LOCATION_HEADERS
    ProximitySensor.h
    LocationSource.h
    DeviceLocationSource.h
    BluetoothScanner.h
    BluetoothDevice.h
    BluetoothSource.h
    ${CMAKE_CURRENT_BINARY_DIR}/location_export.h
)

set_target_properties(Location PROPERTIES
    PUBLIC_HEADER "${LOCATION_HEADERS}"
    VERSION ${PROJECT_VERSION}
    SOVERSION 1
    OUTPUT_NAME "ZaxLocation"
)

generate_export_header(Location)

install(TARGETS Location
    EXPORT ZaxTargets
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/Zax/Location
)

# ecm_add_qml_module(ZaxLocationQmlPlugin URI "Zax.Location")
#
# target_sources(ZaxLocationQmlPlugin PRIVATE QmlPlugin.cpp)
#
# target_link_libraries(ZaxLocationQmlPlugin PRIVATE Location Qt6::Quick)
#
# ecm_finalize_qml_module(ZaxLocationQmlPlugin DESTINATION ${QML_INSTALL_DIR})
