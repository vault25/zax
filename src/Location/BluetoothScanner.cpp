/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "BluetoothScanner.h"

#include <cmath>

#include <QHash>
#include <QTimer>

#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothAddress>
#include <QBluetoothLocalDevice>

#include <QDebug>

#include "BluetoothDevice.h"

using namespace Zax::Location;

class LOCATION_NO_EXPORT BluetoothScanner::Private
{
public:
    Private(BluetoothScanner* qq) : q(qq) { }

    BluetoothDevice* createOrGetDevice(const QBluetoothDeviceInfo& info);

//     void discoveryFinished();

    BluetoothScanner* q = nullptr;

    bool enabled = false;

    std::unique_ptr<QBluetoothDeviceDiscoveryAgent> agent;

    QHash<QString, BluetoothDevice*> devices;
    QHash<QString, int> lastKnownRssi;

    static std::shared_ptr<BluetoothScanner> instance;
};

std::shared_ptr<BluetoothScanner> BluetoothScanner::Private::instance;

BluetoothScanner::BluetoothScanner(QObject* parent)
    : QObject(parent), d(new Private(this))
{

}

BluetoothScanner::~BluetoothScanner()
{
}

bool BluetoothScanner::enabled() const
{
    return d->enabled;
}

void BluetoothScanner::setEnabled(bool newEnabled)
{
    if (newEnabled == d->enabled) {
        return;
    }

    d->enabled = newEnabled;

    if (d->enabled) {
        d->agent = std::make_unique<QBluetoothDeviceDiscoveryAgent>();
        d->agent->setLowEnergyDiscoveryTimeout(1000);

        QBluetoothLocalDevice local;
        local.powerOn();

        connect(d->agent.get(), &QBluetoothDeviceDiscoveryAgent::finished, this, &BluetoothScanner::onDiscoveryFinished);

        d->agent->start();
    } else {
        d->agent->stop();
        d->agent = nullptr;
    }

    Q_EMIT enabledChanged();
}


QList<BluetoothDevice*> BluetoothScanner::devices() const
{
    return d->devices.values();
}

BluetoothDevice* BluetoothScanner::device(const QString& deviceId) const
{
    return d->devices.value(deviceId, nullptr);
}

std::shared_ptr<BluetoothScanner> BluetoothScanner::instance()
{
    if(!Private::instance) {
        Private::instance = std::make_shared<BluetoothScanner>();
    }

    return Private::instance;
}

void BluetoothScanner::onDiscoveryFinished()
{
    const auto discoveredDevices = d->agent->discoveredDevices();
    for(auto info : discoveredDevices)
    {
        if (info.address().isNull()) {
            continue;
        }

        if (info.rssi() == 0) {
            continue;
        }

        auto address = info.address().toString();

        if (d->lastKnownRssi.value(address) == info.rssi()) {
            continue;
        }

        BluetoothDevice* device = nullptr;
        if (!d->devices.contains(address)) {
            device = BluetoothDevice::fromDeviceInfo(info);
            device->setParent(this);
            d->devices.insert(address, device);
            Q_EMIT deviceFound(device);
            Q_EMIT devicesChanged();
        } else {
            device = d->devices.value(address);
        }

        device->setName(info.name());
        device->setRssi(info.rssi());
        device->markAsSeen();
    }

    // This is rather ugly but currently the only way I know how to detect devices
    // that disappeared. Basically, if there was no recent change to RSSI we are
    // seeing a cached value and should remove the device. To prevent the device
    // from reappearing we also cache what the last RSSI value known is so we can
    // filter those out when adding devices.

    auto now = std::chrono::steady_clock::now();

    QStringList toRemove;
    for (auto device : std::as_const(d->devices)) {
        if ((now - device->lastSeen()) > std::chrono::seconds(10)) {
            Q_EMIT deviceLost(device);
            d->lastKnownRssi.insert(device->address(), device->rssi());
            toRemove.append(device->address());
        }
    }

    for (const auto& address : toRemove) {
        d->devices.value(address)->deleteLater();
        d->devices.remove(address);
    }

    d->agent->start();
}

BluetoothDevice* BluetoothScanner::Private::createOrGetDevice(const QBluetoothDeviceInfo& info)
{
    auto address = info.address().toString();

    if (devices.contains(address)) {
        return devices.value(address);
    }

    auto device = BluetoothDevice::fromDeviceInfo(info);
    devices.insert(address, device);

    return device;
}
