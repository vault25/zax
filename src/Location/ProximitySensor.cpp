/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "ProximitySensor.h"

#include <QTimer>
#include <QDebug>

#include "LocationSource.h"

using namespace Zax::Location;

class LOCATION_NO_EXPORT ProximitySensor::Private
{
public:
    std::unique_ptr<QTimer> updateTimer;

    LocationSource* origin = nullptr;

    QVector<LocationSource*> locations;

    std::chrono::milliseconds updateInterval;
};

ProximitySensor::ProximitySensor(QObject* parent)
    : QObject(parent)
    , d(std::make_unique<Private>())
{
    d->updateTimer = std::make_unique<QTimer>();
    d->updateTimer->setSingleShot(false);
    connect(d->updateTimer.get(), &QTimer::timeout, this, &ProximitySensor::update);
}

ProximitySensor::~ProximitySensor() = default;

LocationSource* ProximitySensor::origin() const
{
    return d->origin;
}

void ProximitySensor::setOrigin(LocationSource* newOrigin)
{
    if (newOrigin == d->origin) {
        return;
    }

    d->origin = newOrigin;
    Q_EMIT originChanged();
}

QQmlListProperty<LocationSource> ProximitySensor::locationsProperty()
{
    return QQmlListProperty<LocationSource>(this,
                                            this,
                                            ProximitySensor::appendLocation,
                                            ProximitySensor::locationCount,
                                            ProximitySensor::locationAt,
                                            ProximitySensor::clearLocations,
                                            ProximitySensor::replaceLocation,
                                            ProximitySensor::removeLastLocation);
}

QVector<LocationSource*> ProximitySensor::locations() const
{
    return d->locations;
}

QStringList ProximitySensor::locationIdentifiers() const
{
    QStringList result;
    std::transform(d->locations.cbegin(), d->locations.cend(), std::back_inserter(result), [](auto location) {
        return location->identifier();
    });
    return result;
}

void ProximitySensor::setLocations(const QVector<LocationSource*>& newLocations)
{
    if (newLocations == d->locations) {
        return;
    }

    d->locations = newLocations;
    Q_EMIT locationsChanged();
}

void ProximitySensor::addLocation(LocationSource* location)
{
    d->locations.append(location);
}

void ProximitySensor::removeLocation(LocationSource* location)
{
    auto itr = std::find(d->locations.begin(), d->locations.end(), location);
    if (itr != d->locations.end()) {
        (*itr)->deleteLater();
        d->locations.erase(itr);
    }
}

void ProximitySensor::removeLocation(const QString& identifier)
{
    auto itr = std::find_if(d->locations.begin(), d->locations.end(), [identifier](auto location) {
        return location->identifier() == identifier;
    });
    if (itr != d->locations.end()) {
        (*itr)->deleteLater();
        d->locations.erase(itr);
    }
}

int ProximitySensor::updateInterval() const
{
    return d->updateInterval.count();
}

void ProximitySensor::setUpdateInterval(int newUpdateInterval)
{
    setUpdateInterval(std::chrono::milliseconds(newUpdateInterval));
}

void ProximitySensor::setUpdateInterval(std::chrono::milliseconds newUpdateInterval)
{
    if (newUpdateInterval == d->updateInterval) {
        return;
    }

    d->updateInterval = newUpdateInterval;
    d->updateTimer->setInterval(d->updateInterval);

    if (d->updateInterval > std::chrono::milliseconds::zero()) {
        d->updateTimer->start();
    } else {
        d->updateTimer->stop();
    }

    Q_EMIT updateIntervalChanged();
}

void ProximitySensor::update()
{
    if (!d->origin) {
        return;
    }

    for (auto location : std::as_const(d->locations)) {
        auto distance = location->distanceTo(d->origin);

        if (location->triggerDistance() >= 0.0 && distance > location->triggerDistance()) {
            continue;
        }

        if (distance >= 0.0) {
            auto azimuth = location->azimuthTo(d->origin);

            Q_EMIT detected(location, distance, azimuth);
        }
    }
}

void ProximitySensor::appendLocation(QQmlListProperty<LocationSource>* property, LocationSource* location)
{
    auto query = qobject_cast<ProximitySensor*>(property->object);
    query->d->locations.append(location);
    Q_EMIT query->locationsChanged();
}

qsizetype ProximitySensor::locationCount(QQmlListProperty<LocationSource>* property)
{
    auto query = qobject_cast<ProximitySensor*>(property->object);
    return query->d->locations.size();
}

LocationSource* ProximitySensor::locationAt(QQmlListProperty<LocationSource>* property, qsizetype index)
{
    auto query = qobject_cast<ProximitySensor*>(property->object);
    return query->d->locations.at(index);
}

void ProximitySensor::clearLocations(QQmlListProperty<LocationSource>* property)
{
    auto query = qobject_cast<ProximitySensor*>(property->object);
    query->d->locations.clear();
    Q_EMIT query->locationsChanged();
}

void ProximitySensor::replaceLocation(QQmlListProperty<LocationSource>* property, qsizetype index, LocationSource* location)
{
    auto query = qobject_cast<ProximitySensor*>(property->object);
    query->d->locations.replace(index, location);
    Q_EMIT query->locationsChanged();
}

void ProximitySensor::removeLastLocation(QQmlListProperty<LocationSource>* property)
{
    auto query = qobject_cast<ProximitySensor*>(property->object);
    query->d->locations.removeLast();
    Q_EMIT query->locationsChanged();
}
