/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <chrono>

#include <QObject>
#include <QQmlListProperty>

#include "location_export.h"

namespace Zax {
namespace Location {

class LocationSource;

class LOCATION_EXPORT ProximitySensor : public QObject
{
    Q_OBJECT

public:
    ProximitySensor(QObject* parent = nullptr);
    ~ProximitySensor() override;

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(Zax::Location::LocationSource* origin READ origin WRITE setOrigin NOTIFY originChanged)
    LocationSource* origin() const;
    void setOrigin(LocationSource* newOrigin);
    Q_SIGNAL void originChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(QQmlListProperty<Zax::Location::LocationSource> locations READ locationsProperty NOTIFY locationsChanged)
    QQmlListProperty<LocationSource> locationsProperty();
    QVector<LocationSource*> locations() const;
    QStringList locationIdentifiers() const;
    void setLocations(const QVector<LocationSource*>& newlocations);
    Q_SIGNAL void locationsChanged();

    Q_INVOKABLE void addLocation(Zax::Location::LocationSource* location);
    Q_INVOKABLE void removeLocation(Zax::Location::LocationSource* location);
    Q_INVOKABLE void removeLocation(const QString& identifier);

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(int updateInterval READ updateInterval WRITE setUpdateInterval NOTIFY updateIntervalChanged)
    int updateInterval() const;
    void setUpdateInterval(int newUpdateInterval);
    void setUpdateInterval(std::chrono::milliseconds interval);
    Q_SIGNAL void updateIntervalChanged();

    Q_INVOKABLE void update();

    Q_SIGNAL void detected(Zax::Location::LocationSource* location, qreal distance, qreal azimuth);

private:

    static void appendLocation(QQmlListProperty<LocationSource>* property, LocationSource* rule);
    static qsizetype locationCount(QQmlListProperty<LocationSource>* property);
    static LocationSource* locationAt(QQmlListProperty<LocationSource>* property, qsizetype index);
    static void clearLocations(QQmlListProperty<LocationSource>* property);
    static void replaceLocation(QQmlListProperty<LocationSource>* property, qsizetype index, LocationSource* rule);
    static void removeLastLocation(QQmlListProperty<LocationSource>* property);

    class Private;
    const std::unique_ptr<Private> d;
};

} // Location
} // Zax
