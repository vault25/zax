/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <QGeoPositionInfo>

#include "LocationSource.h"

#include "location_export.h"

namespace Zax {
namespace Location {

class LOCATION_EXPORT DeviceLocationSource : public LocationSource
{
    Q_OBJECT

public:
    DeviceLocationSource(QObject* parent = nullptr);
    ~DeviceLocationSource() override;

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(int updateInterval READ updateInterval WRITE setUpdateInterval NOTIFY updateIntervalChanged)
    int updateInterval() const;
    void setUpdateInterval(int newUpdateInterval);
    void setUpdateInterval(std::chrono::milliseconds newUpdateInterval);
    Q_SIGNAL void updateIntervalChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(qreal minimumAccuracy READ minimumAccuracy WRITE setMinimumAccuracy NOTIFY minimumAccuracyChanged)
    qreal minimumAccuracy() const;
    void setMinimumAccuracy(qreal newMinimumAccuracy);
    Q_SIGNAL void minimumAccuracyChanged();

    /**
    * TODO: Documentation
    */
    Q_PROPERTY(qreal accuracy READ accuracy NOTIFY coordinateChanged)
    qreal accuracy() const;

private:
    void onPositionUpdated(const QGeoPositionInfo& position);

    class Private;
    const std::unique_ptr<Private> d;
};

} // namespace Location
} // namespace Zax
