import QtQuick 2.15
import QtQuick.Controls 2.15

import Zax.Core 1.0 as Core
import Zax.Location 1.0 as Location

Core.Application {
    states: [
        Core.ApplicationState {
            page: initialPage
            state: Core.Application.State.Operational
        }
    ]

    Component {
        id: initialPage

        Page {
            ListView {
                anchors.fill: parent

                model: Location.BluetoothScanner.devices

                delegate: ItemDelegate {
                    text: modelData.name + " (" + modelData.address + "): " + modelData.rssi + " " + modelData.distance
                }
            }

            Component.onCompleted: Location.BluetoothScanner.enabled = true
        }
    }
}
