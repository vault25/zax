
if (BUILD_JSONAPI)
    add_subdirectory(jsonapi)
endif()

if (BUILD_LOCATION)
    add_subdirectory(location)
endif()
