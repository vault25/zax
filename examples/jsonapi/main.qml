import QtQuick
import QtQuick.Controls

import Zax.Core as Core
import Zax.JsonApi as JsonApi

Core.Application {
    states: [
        Core.ApplicationState {
            page: initialPage
            state: Core.ApplicationState.Opening
            duration: 1000
        },
        Core.ApplicationState {
            page: "qrc:/Operational.qml"
            state: Core.ApplicationState.Operational
        }
    ]

    Component {
        id: initialPage

        Rectangle {
            color: "red"

            Label {
                anchors.centerIn: parent
                text: "Initial"
            }
        }
    }
}
