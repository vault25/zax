import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import Zax.Core as Core
import Zax.JsonApi as JsonApi

RowLayout {
    ScrollView {
        Layout.fillWidth: true
        Layout.preferredWidth: 1
        Layout.fillHeight: true

        ListView {
            id: listView

            header: ToolBar {
                RowLayout {
                    anchors.fill: parent

                    ComboBox {
                        model: [
                            {text: "Title", value: "title" },
                            {text: "ID", value: "id"},
                            {text: "Changed", value: "changed"}
                        ]

                        textRole: "text"
                        valueRole: "value"

                        onActivated: sortRule.field = currentValue
                    }

                    Button {
                        text: "Sort: %1".arg(sortRule.direction == JsonApi.SortRule.Descending ? "Descending" : "Ascending")
                        onClicked: sortRule.direction = sortRule.direction == JsonApi.SortRule.Descending ? JsonApi.SortRule.Ascending : JsonApi.SortRule.Descending
                    }
                }
            }

            model: JsonApi.ApiModel {
                id: itemModel

                path: "node/wiki_page"
                attributes: {
                    "title": "string",
                    "body": "string",
                    "changed": "date"
                }

                JsonApi.FilterRule { field: "status"; value: 1 }
                JsonApi.SortRule { id: sortRule; field: "changed"; direction: JsonApi.SortRule.Descending }
                JsonApi.IncludeRule { field: "uid"; }
            }

            delegate: ItemDelegate {
                width: ListView.view.width
                text: model.title

                checked: ListView.isCurrentItem
                onClicked: ListView.view.currentIndex = index
            }
        }
    }

    ScrollView {
        Layout.fillWidth: true
        Layout.preferredWidth: 2
        Layout.fillHeight: true

        TextArea {
            width: parent.width
            text: itemModel.get(listView.currentIndex).values.body
            textFormat: TextEdit.MarkdownText
            wrapMode: Text.WordWrap
        }
    }
}
