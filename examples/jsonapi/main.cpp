#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "JsonApi/Api.h"

int main(int argc, char** argv)
{
    QGuiApplication app(argc, argv);

    Zax::JsonApi::Api::instance()->setServers({
        QStringLiteral("https://convergence-larp.nl/")
    });
    Zax::JsonApi::Api::instance()->start();

    QQmlApplicationEngine engine(QUrl(QStringLiteral("qrc:/main.qml")));
    return app.exec();
}
