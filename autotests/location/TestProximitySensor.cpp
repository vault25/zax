/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <QTest>
#include <QSignalSpy>

#include "Location/ProximitySensor.h"
#include "Location/LocationSource.h"

using namespace Zax::Location;

class TestProximitySensor : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void initTestCase()
    {
        qRegisterMetaType<Zax::Location::LocationSource*>();
    }

    void test_proximity_data()
    {
        QTest::addColumn<QGeoCoordinate>("origin");
        QTest::addColumn<QVector<QGeoCoordinate>>("locations");
        QTest::addColumn<int>("detections");

        QTest::newRow("same location") << QGeoCoordinate{0.0, 0.0} << QVector{QGeoCoordinate{0.0, 0.0}} << 1;
        QTest::newRow("near") << QGeoCoordinate{10.0, 10.0} << QVector{QGeoCoordinate{10.0001, 10.0001}} << 1;
        QTest::newRow("far") << QGeoCoordinate{0.0, 0.0} << QVector{QGeoCoordinate{50.0, 50.0}} << 0;
        QTest::newRow("multiple") << QGeoCoordinate{-10.0, -10.0}
                                  << QVector{QGeoCoordinate{-10.0001, -10.0}, QGeoCoordinate{-10.0, -10.0001}}
                                  << 2;
    }

    void test_proximity()
    {
        auto originSource = std::make_unique<LocationSource>();
        QFETCH(QGeoCoordinate, origin);
        originSource->setCoordinate(origin);

        QVector<LocationSource*> locationSources;
        QFETCH(const QVector<QGeoCoordinate>, locations);
        for (auto coord : locations) {
            auto newLocation = new LocationSource{};
            newLocation->setCoordinate(coord);
            newLocation->setTriggerDistance(100.0);
            locationSources.append(newLocation);
        }

        auto sensor = std::make_unique<ProximitySensor>();
        sensor->setOrigin(originSource.get());
        sensor->setLocations(locationSources);

        QSignalSpy detectionSpy(sensor.get(), &ProximitySensor::detected);
        QFETCH(int, detections);

        sensor->update();

        QCOMPARE(detectionSpy.count(), detections);

        qDeleteAll(locationSources);
    }

    void test_changingLocations()
    {
        auto originSource = std::make_unique<LocationSource>();
        originSource->setCoordinate(QGeoCoordinate{0.0, 0.0});

        auto sensor = std::make_unique<ProximitySensor>();
        sensor->setOrigin(originSource.get());

        QSignalSpy detectionSpy(sensor.get(), &ProximitySensor::detected);

        for (int t = 0; t < 5; ++t) {
            QVector<LocationSource*> locations;
            for (int i = 0; i < 5; ++i) {
                auto newLocation = new LocationSource{};
                newLocation->setCoordinate(QGeoCoordinate{qreal(i), qreal(i)});
                locations << newLocation;
            }

            sensor->setLocations(locations);
            sensor->update();

            QCOMPARE(detectionSpy.count(), 5);

            qDeleteAll(locations);
            detectionSpy.clear();
        }
    }

    void test_addRemove()
    {
        auto originSource = std::make_unique<LocationSource>();
        originSource->setCoordinate(QGeoCoordinate{0.0, 0.0});

        auto sensor = std::make_unique<ProximitySensor>();
        sensor->setOrigin(originSource.get());

        LocationSource* location = new LocationSource{};

        sensor->addLocation(location);

        QCOMPARE(sensor->locations().size(), 1);

        sensor->removeLocation(location);

        QCOMPARE(sensor->locations().size(), 0);
    }
};

QTEST_MAIN(TestProximitySensor)

#include "TestProximitySensor.moc"
