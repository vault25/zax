/*
 * SPDX-FileCopyrightText: 2023 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <JsonApi/Api.h>
#include <JsonApi/Request.h>
#include <qtestcase.h>

#include "JsonApiTest.h"

class TestRequest : public JsonApiTest
{
    Q_OBJECT

private Q_SLOTS:
    void testPost()
    {
        auto request = makeRequest();
        ApiSpy spy(request.get());

        request->setMethod(Request::Post);
        request->setPath(u"test"_qs);

        request->setData(QVariantMap{
            {u"title"_qs, u"Posted Entry"_qs},
            {u"bool_value"_qs, true},
            {u"int_value"_qs, 9},
        });

        request->execute();

        if (!spy.wait()) {
            QFAIL(spy.error().data());
        }

        auto result = Api::instance()->get(u"test/2", QString{}, Cache::CacheMode::DoNotCache);
        QVERIFY(waitForResult(result));
        QCOMPARE(result->errorCode(), QNetworkReply::NoError);
    }

    void testPatch()
    {
        auto request = makeRequest();
        ApiSpy spy(request.get());

        request->setMethod(Request::Patch);
        request->setPath(u"test/1"_qs);

        request->setData(QVariantMap{
            {u"title"_qs, u"Patched Entry"_qs},
            {u"bool_value"_qs, true},
            {u"int_value"_qs, 9},
        });

        request->execute();

        if (!spy.wait()) {
            QFAIL(spy.error().data());
        }

        auto result = Api::instance()->get(u"test/1", QString{}, Cache::CacheMode::DoNotCache);
        QVERIFY(waitForResult(result));
        QCOMPARE(result->errorCode(), QNetworkReply::NoError);

        auto json = result->asJson().value_or(QJsonDocument{});
        auto attributes = json[u"data"][u"attributes"].toObject();

        QCOMPARE(attributes.value(u"title").toString(), u"Patched Entry"_qs);
        QCOMPARE(attributes.value(u"bool_value").toBool(), true);
        QCOMPARE(attributes.value(u"int_value").toInt(), 9);
    }

    void testPatchMultiple()
    {
        auto request = makeRequest();
        ApiSpy spy(request.get());

        request->setMethod(Request::Patch);
        request->setPaths({
            u"test/0"_qs,
            u"test/1"_qs,
        });
        request->setData(QVariantList{
            QVariantMap{
                {u"title"_qs, u"Patched Entry 0"_qs},
                {u"int_value"_qs, 10}
            },
            QVariantMap{
                {u"title"_qs, u"Patched Entry 1"_qs},
                {u"int_value"_qs, 11}
            }
        });

        request->execute();

        if (!spy.wait()) {
            QFAIL(spy.error().data());
        }

        auto result = Api::instance()->get(u"test", QString{}, Cache::CacheMode::DoNotCache);
        QVERIFY(waitForResult(result));
        QCOMPARE(result->errorCode(), QNetworkReply::NoError);

        auto json = result->asJson().value_or(QJsonDocument{});
        auto entries = json[u"data"].toArray();
        QCOMPARE(entries.size(), 2);

        for (int i = 0; i < entries.size(); ++i) {
            QCOMPARE(entries.at(i)[u"attributes"][u"title"], u"Patched Entry %1"_qs.arg(i));
        }
    }

    void testDelete()
    {
        auto request = makeRequest();
        ApiSpy spy(request.get());

        request->setMethod(Request::Delete);
        request->setPath(u"test/1"_qs);

        request->execute();

        if (!spy.wait()) {
            QFAIL(spy.error().data());
        }

        auto result = Api::instance()->get(u"test/1", QString{}, Cache::CacheMode::DoNotCache);
        QVERIFY(waitForResult(result));
        QCOMPARE(result->errorCode(), QNetworkReply::ContentNotFoundError);
    }

    void testDeleteMultiple()
    {
        auto request = makeRequest();
        ApiSpy spy(request.get());

        request->setMethod(Request::Delete);
        request->setPaths({
            u"test/1"_qs,
            u"test/0"_qs,
        });

        request->execute();

        if (!spy.wait()) {
            QFAIL(spy.error().data());
        }

        auto result = Api::instance()->get(u"test", QString{}, Cache::CacheMode::DoNotCache);
        QVERIFY(waitForResult(result));
        QVERIFY(result->asJson().value_or(QJsonDocument{})[u"data"].toArray().isEmpty());
    }

private:
    std::unique_ptr<Request> makeRequest()
    {
        auto request = std::make_unique<Request>();
        request->setType(u"test"_qs);
        request->setAttributes({
            {u"title"_qs, u"string"_qs},
            {u"bool_value"_qs, u"bool"_qs},
            {u"int_value"_qs, u"int"_qs},
        });
        return request;
    }
};

QTEST_MAIN(TestRequest)

#include "TestRequest.moc"
