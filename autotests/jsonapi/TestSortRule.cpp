/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <JsonApi/Api.h>
#include <JsonApi/SortRule.h>

#include <QtTest>
#include <QStandardPaths>

using namespace Zax::JsonApi;

class TestSortRule : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testBasic()
    {
        auto rule = std::make_unique<SortRule>();

        QUrlQuery query;
        rule->modifyQuery(query);

        QVERIFY(query.isEmpty());
    }

    void testSingleField_data()
    {
        QTest::addColumn<QString>("field");
        QTest::addColumn<SortRule::Direction>("direction");
        QTest::addColumn<QString>("result");

        QTest::newRow("id") << u"id"_qs << SortRule::Ascending << u"sort=id"_qs;
        QTest::newRow("name") << u"name"_qs << SortRule::Ascending << u"sort=name"_qs;
        QTest::newRow("name reversed") << u"name"_qs << SortRule::Descending << u"sort=-name"_qs;
    }

    void testSingleField()
    {
        auto rule = std::make_unique<SortRule>();

        QFETCH(QString, field);
        QFETCH(SortRule::Direction, direction);
        QFETCH(QString, result);

        rule->setField(field);
        rule->setDirection(direction);

        QUrlQuery query;
        rule->modifyQuery(query);

        QVERIFY(!query.isEmpty());
        QCOMPARE(query.toString(), result);
    }

    void testMultiField_data()
    {
        QTest::addColumn<QStringList>("fields");
        QTest::addColumn<SortRule::Direction>("direction");
        QTest::addColumn<QString>("result");

        QTest::newRow("id, name") << QStringList{u"id"_qs, u"name"_qs} << SortRule::Ascending << u"sort=id,name"_qs;
        QTest::newRow("id, name reversed") << QStringList{u"id"_qs, u"name"_qs} << SortRule::Descending << u"sort=-id,name"_qs;
    }

    void testMultiField()
    {
        auto rule = std::make_unique<SortRule>();

        QFETCH(QStringList, fields);
        QFETCH(SortRule::Direction, direction);
        QFETCH(QString, result);

        rule->setFields(fields);
        rule->setDirection(direction);

        QUrlQuery query;
        rule->modifyQuery(query);

        QVERIFY(!query.isEmpty());
        QCOMPARE(query.toString(), result);
    }
};

QTEST_MAIN(TestSortRule)

#include "TestSortRule.moc"
