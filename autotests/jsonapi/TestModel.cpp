/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <JsonApi/Api.h>
#include <JsonApi/ApiModel.h>

#include "JsonApiTest.h"

class TestModel : public JsonApiTest
{
    Q_OBJECT

private Q_SLOTS:
    void testBasic()
    {
        auto model = std::make_unique<ApiModel>();
        QAbstractItemModelTester tester(model.get());

        ApiSpy spy(model->query());

        model->setPath(u"test"_qs);
        model->setAttributes({
            {u"title"_qs, u"string"_qs},
            {u"bool_value"_qs, u"bool"_qs},
            {u"int_value"_qs, u"int"_qs},
        });

        model->componentComplete();

        if (!spy.wait()) {
            QFAIL(spy.error().data());
        }

        QCOMPARE(model->rowCount(), 2);
        auto roleNames = model->roleNames();
        QCOMPARE(roleNames.count(), 5);

        for (int i = 0; i < model->rowCount(); ++i) {
            auto index = model->index(i, 0);
            auto title = model->data(index, roleNames.key("title"));
            QVERIFY(title.isValid());
            QCOMPARE(title.toString(), u"Test %1"_qs.arg(i));

            auto intValue = model->data(index, roleNames.key("int_value"));
            QVERIFY(intValue.isValid());
            QCOMPARE(intValue.toInt(), i);
        }
    }
};

QTEST_MAIN(TestModel)

#include "TestModel.moc"
