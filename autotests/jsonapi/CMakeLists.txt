
add_executable(MockServer MockServer.cpp)
target_link_libraries(MockServer Qt6::HttpServer)

ModuleTest(JsonApi Api JsonApiTest.cpp)
ModuleTest(JsonApi Query JsonApiTest.cpp)
ModuleTest(JsonApi Model JsonApiTest.cpp)
ModuleTest(JsonApi SortRule)
ModuleTest(JsonApi Request JsonApiTest.cpp)
