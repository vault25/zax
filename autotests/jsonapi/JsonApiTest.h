/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <QtTest>
#include <QStandardPaths>

#include <JsonApi/Api.h>
#include <JsonApi/Query.h>

#include "Config.h"

using namespace Zax::JsonApi;

template <typename T>
class ApiSpy : public QObject
{
public:
    ApiSpy(T *object)
        : m_object(object)
    {
        QObject::connect(m_object, &T::success, [this]() {
            m_finished = true;
        });
        QObject::connect(m_object, &T::error, [this]() {
            m_finished = true;
            m_error = true;
        });
    }

    [[nodiscard]] bool wait(int timeout = 5000)
    {
        auto result = QTest::qWaitFor([this]() {
            return m_finished;
        }, timeout);

        if (!result) {
            m_timeout = true;
            return false;
        }

        if (m_error) {
            return false;
        }

        return true;
    }

    QByteArray error() const
    {
        if (m_timeout) {
            return "Timed out waiting for query to run";
        }

        return m_object->result()->errorString().toUtf8();
    }

private:
    T* m_object;
    bool m_finished = false;
    bool m_error = false;
    bool m_timeout = false;
};

class JsonApiTest : public QObject
{
    Q_OBJECT

public Q_SLOTS:
    void initTestCase();
    void init();
    void cleanup();

protected:
    bool waitForResult(ApiResultPtr& result);

    std::shared_ptr<Zax::JsonApi::Api> m_api;
    QProcess m_mockServer;
};
