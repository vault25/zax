/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "JsonApiTest.h"

class TestApi : public JsonApiTest
{
    Q_OBJECT

private Q_SLOTS:
    void testGetJsonNoCache()
    {
        auto result = getJson(u"test"_qs, QString{}, Cache::CacheMode::DoNotCache, __FILE__, __LINE__);
        if (!result.has_value()) {
            return;
        }

        auto json = result.value();
        QVERIFY(json.isObject());

        auto data = json[u"data"];
        QVERIFY(data.isArray());
        QVERIFY(data.toArray().size() == 2);
    }

    void testGetJsonCache()
    {
        auto result1 = getJson(u"test"_qs, QString{}, Cache::CacheMode::Cache, __FILE__, __LINE__);
        if (!result1.has_value()) {
            return;
        }

        auto json1 = result1.value();
        QVERIFY(json1.isObject());

        auto cacheUrl = QUrl(u"http://localhost:45454/jsonapi/test"_qs);
        QVERIFY(m_api->cache()->isValidFor(cacheUrl));

        auto result2 = getJson(u"test"_qs, QString{}, Cache::CacheMode::CacheOnly, __FILE__, __LINE__);
        if (!result2.has_value()) {
            return;
        }

        auto json2 = result2.value();

        QCOMPARE(json1, json2);
    }

    void testGetSingle()
    {
        auto result = getJson(u"test/0"_qs, QString{}, Cache::CacheMode::DoNotCache, __FILE__, __LINE__);
        if (!result.has_value()) {
            return;
        }

        auto json = result.value();
        QVERIFY(json.isObject());

        auto data = json[u"data"];
        QVERIFY(data.isObject());
    }

    void testPost()
    {
        QJsonObject document{
            {u"data"_qs, QJsonObject{
                {u"id"_qs, 2},
                {u"type"_qs, u"test"_qs},
                {u"attributes"_qs, QJsonObject{
                    {u"title"_qs, u"Posted Item"_qs},
                    {u"bool_value"_qs, false},
                    {u"int_value"_qs, 2},
                }}
            }}
        };

        auto result = m_api->post(u"test"_qs, document);
        QVERIFY(waitForResult(result));
        QCOMPARE(result->errorCode(), QNetworkReply::NoError);

        auto get = getJson(u"test/2"_qs, QString{}, Cache::CacheMode::DoNotCache, __FILE__, __LINE__);
        QVERIFY(get.has_value());

        auto json = get.value();
        QVERIFY(json.isObject());

        auto data = json[u"data"];
        QVERIFY(data.isObject());
        QCOMPARE(data[u"id"].toInt(), 2);
    }

    void testPatch()
    {
        QJsonObject document{
            {u"data"_qs, QJsonObject{
                {u"type"_qs, u"test"_qs},
                {u"attributes"_qs, QJsonObject{
                    {u"bool_value"_qs, true},
                    {u"int_value"_qs, 2},
                }}
            }}
        };

        auto result = m_api->patch(u"test/1"_qs, document);
        QVERIFY(waitForResult(result));
        QCOMPARE(result->errorCode(), QNetworkReply::NoError);

        auto get = getJson(u"test/1"_qs, QString{}, Cache::CacheMode::DoNotCache, __FILE__, __LINE__);
        QVERIFY(get.has_value());

        auto json = get.value();
        QVERIFY(json.isObject());

        auto data = json[u"data"];
        QVERIFY(data.isObject());
        QCOMPARE(data[u"id"].toInt(), 1);
        QCOMPARE(data[u"attributes"].toObject().value(u"bool_value").toBool(), true);
        QCOMPARE(data[u"attributes"].toObject().value(u"int_value").toInt(), 2);
    }

    void testDelete()
    {
        auto result = m_api->del(u"test/1"_qs, QJsonObject{});
        QVERIFY(waitForResult(result));
        QCOMPARE(result->errorCode(), QNetworkReply::NoError);

        auto get = m_api->get(u"test/1", QString{}, Cache::CacheMode::DoNotCache);
        QVERIFY(waitForResult(get));
        QCOMPARE(get->errorCode(), QNetworkReply::ContentNotFoundError);
    }

private:
    std::optional<QJsonDocument> getJson(QStringView path, QStringView filter, Cache::CacheMode cache, const char* file, int line)
    {
        auto result = m_api->get(path, filter, cache);
        if (!waitForResult(result)) {
            QTest::qFail("Timeout retrieving data", file, line);
            return std::nullopt;
        }

        if (result->errorCode() != QNetworkReply::NoError) {
            QTest::qFail(result->errorString().toUtf8().data(), file, line);
            return std::nullopt;
        }

        auto json = result->asJson();
        if (!json.has_value()) {
            QTest::qFail("No JSON document provided", file, line);
            return std::nullopt;
        }

        return json.value();
    }
};

QTEST_MAIN(TestApi)

#include "TestApi.moc"
