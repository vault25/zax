/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <JsonApi/Api.h>
#include <JsonApi/Query.h>

#include "JsonApiTest.h"

class TestQuery : public JsonApiTest
{
    Q_OBJECT

private Q_SLOTS:
    void testBasic()
    {
        auto query = std::make_unique<Query>();
        ApiSpy spy(query.get());

        query->setPath(u"test"_qs);
        query->run();

        if (!spy.wait()) {
            QFAIL(spy.error().data());
        }

        const auto entries = query->entries();

        QCOMPARE(entries.size(), 2);

        for (int i = 0; i < entries.size(); ++i) {
            auto entry = entries.at(i).toMap();
            QCOMPARE(entry.value(u"title"_qs).toString(), u"Test %1"_qs.arg(i));
            QCOMPARE(entry.value(u"int_value"_qs).toInt(), i);
        }
    }
};

QTEST_MAIN(TestQuery)

#include "TestQuery.moc"
