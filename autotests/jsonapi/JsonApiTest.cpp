/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "JsonApiTest.h"

void JsonApiTest::initTestCase()
{
    QStandardPaths::setTestModeEnabled(true);
    m_mockServer.setProgram(QString::fromUtf8(CMAKE_BINARY_DIR "/bin/MockServer"));
    m_mockServer.setProcessChannelMode(QProcess::ForwardedChannels);
}

void JsonApiTest::init()
{
    m_mockServer.start();
    if (!m_mockServer.waitForStarted()) {
        QFAIL("Could not start Mock JsonApi server");
    }

    QTest::qWait(100);

    m_api = Zax::JsonApi::Api::instance();
    m_api->setServers({u"http://localhost:26124"_qs});
    m_api->start();

    auto success = QTest::qWaitFor([this]() {
        return m_api->isConnected();
    });

    if (!success) {
        QFAIL("Cannot connect to server");
    }
}

void JsonApiTest::cleanup()
{
    m_api->cache()->clear();
    m_mockServer.terminate();
    m_mockServer.waitForFinished();
}

bool JsonApiTest::waitForResult(Zax::JsonApi::ApiResultPtr& result)
{
    bool finished = false;
    bool error = false;

    connect(result.get(), &ApiResult::success, this, [&finished]() {
        finished = true;
    });
    connect(result.get(), &ApiResult::error, this, [&finished, &error]() {
        error = true;
        finished = true;
    });

    auto success = QTest::qWaitFor([&finished]() {
        return finished;
    });

    m_api->cache()->waitForFinished();

    if (!success) {
        return false;
    }

    return true;
}
