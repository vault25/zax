/*
 * SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QHostAddress>
#include <QDebug>

#include <QHttpServer>
#include <QHttpServerResponse>

QHttpServerResponse jsonApiResponse(const QJsonValue& data)
{
    auto jsonApi = QJsonObject{
        {u"data"_qs, data},
        {u"jsonapi"_qs, QJsonObject{
            {u"version"_qs, u"1.0"_qs},
            {u"meta"_qs, QJsonObject{
                {u"links"_qs, QJsonObject{
                    {u"self"_qs, QJsonObject{
                        {u"href"_qs, u"http://jsonapi.org/format/1.0/"_qs}
                    }}
                }}
            }}
        }}
    };
    QHttpServerResponse response{jsonApi};
    response.setHeader("Content-Type", "application/vnd.api+json");
    return response;
}

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);

    QHttpServer httpServer;

    httpServer.route(u"/jsonapi"_qs, []() {
        return jsonApiResponse(QJsonArray{});
    });

    auto testArray = QJsonArray{
        QJsonObject{
            {u"type"_qs, u"test"_qs},
            {u"id"_qs, 0},
            {u"attributes"_qs, QJsonObject{
                {u"title"_qs, u"Test 0"_qs},
                {u"bool_value"_qs, true},
                {u"int_value"_qs, 0},
            }}
        },
        QJsonObject{
            {u"type"_qs, u"test"_qs},
            {u"id"_qs, 1},
            {u"attributes"_qs, QJsonObject{
                {u"title"_qs, u"Test 1"_qs},
                {u"bool_value"_qs, false},
                {u"int_value"_qs, 1},
            }}
        }
    };

    httpServer.route(u"/jsonapi/test"_qs, QHttpServerRequest::Method::Get, [&testArray]() {
        return jsonApiResponse(testArray);
    });
    httpServer.route(u"/jsonapi/test"_qs, QHttpServerRequest::Method::Post, [&testArray](const QHttpServerRequest& request) {
        auto data = QJsonDocument::fromJson(request.body()).object().value(u"data");
        if (!data.isObject()) {
            return QHttpServerResponse(QHttpServerResponse::StatusCode::BadRequest);
        }

        auto object = data.toObject();
        if (!object.contains(u"id")) {
            object[u"id"] = testArray.size();
        }
        testArray.append(object);

        QHttpServerResponse response(QHttpServerResponse::StatusCode::Created);
        response.setHeader("Content-Type", "application/vnd.api+json");
        response.setHeader("Location", (u"/jsonapi/test/%1"_qs.arg(object.value(u"id").toString())).toUtf8());
        return response;
    });

    httpServer.route(u"/jsonapi/test/<arg>"_qs, QHttpServerRequest::Method::Get, [&testArray](int index) {
        if (index < 0 || index >= testArray.count()) {
            return QHttpServerResponse(QHttpServerResponse::StatusCode::NotFound);
        }

        auto object = testArray[index];
        if (object[u"id"] != index) {
            return QHttpServerResponse(QHttpServerResponse::StatusCode::InternalServerError);
        }

        return jsonApiResponse(object);
    });

    httpServer.route(u"/jsonapi/test/<arg>"_qs, QHttpServerRequest::Method::Patch, [&testArray](int index, const QHttpServerRequest& request) {
        if (index < 0 || index >= testArray.count()) {
            return QHttpServerResponse(QHttpServerResponse::StatusCode::NotFound);
        }

        auto existing = testArray.at(index).toObject();
        if (existing[u"id"] != index) {
            return QHttpServerResponse(QHttpServerResponse::StatusCode::InternalServerError);
        }

        auto data = QJsonDocument::fromJson(request.body()).object().value(u"data");
        if (!data.isObject()) {
            return QHttpServerResponse(QHttpServerResponse::StatusCode::BadRequest);
        }

        auto object = data.toObject();
        auto newAttributes = object.value(u"attributes").toObject();
        auto existingAttributes = existing.value(u"attributes").toObject();

        const auto keys = newAttributes.keys();
        for (auto key : keys) {
            existingAttributes[key] = newAttributes[key];
        }
        existing[u"attributes"] = existingAttributes;
        testArray[index] = existing;

        return jsonApiResponse(testArray[index]);
    });

    httpServer.route(u"/jsonapi/test/<arg>"_qs, QHttpServerRequest::Method::Delete, [&testArray](int index) {
        if (index < 0 || index >= testArray.count()) {
            return QHttpServerResponse(QHttpServerResponse::StatusCode::NotFound);
        }

        auto object = testArray[index];
        if (object[u"id"] != index) {
            qDebug() << "Object at index" << index << "does not match ID";
            return QHttpServerResponse(QHttpServerResponse::StatusCode::InternalServerError);
        }
        testArray.removeAt(index);

        return QHttpServerResponse(QHttpServerResponse::StatusCode::NoContent);
    });

    const auto port = httpServer.listen(QHostAddress::Any, 26124);
    if (!port) {
        qDebug() << "Server failed to listen on a port.";
        return 1;
    }

    // qDebug() << QStringLiteral("Running on http://127.0.0.1:%1/ (Press CTRL+C to quit)").arg(port);

    return app.exec();
}
