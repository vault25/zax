FROM  registry.gitlab.com/vault25/qt-android:5.15

COPY . /build/zax

RUN mkdir -p /build/zax/build \
 && cd /build/zax/build \
 && /opt/cmake/bin/cmake .. \
        -DCMAKE_TOOLCHAIN_FILE=/opt/libraries/share/ECM/toolchain/Android.cmake \
        -DCMAKE_INSTALL_PREFIX=/opt/libraries \
        -DCMAKE_PREFIX_PATH=/opt/libraries \
        -DCMAKE_BUILD_TYPE=Release \
        -DBUILD_EXAMPLES=OFF \
        -DBUILD_TESTING=OFF \
 && make install \
 && rm -r /build/zax

